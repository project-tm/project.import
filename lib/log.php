<?php

namespace Project\Import;

class Log {

    public static function clear() {
        unset($_SESSION[__CLASS__]);
    }

    public static function get() {
        if (isset($_SESSION[__CLASS__])) {
            $data = $_SESSION[__CLASS__];
//            unset($_SESSION[__CLASS__]);
            foreach ($data as $key => &$value) {
                if (is_array($value)) {
                    $value = implode(', ', $value);
                }
                $value = $key . ': ' . $value;
            }
            return $data;
        } else {
            return array();
        }
    }

    public static function error($header, $message = '') {
        return self::add(false, $header, $message);
    }

    public static function success($header, $message) {
        return self::add(true, $header, $message);
    }

    private static function add($isOk, $header, $message) {
        if (empty($_SESSION[__CLASS__])) {
            $_SESSION[__CLASS__] = array();
        }
        $header = '<font style="color:' . ($isOk ? 'green' : 'red') . ';">' . $header . '</font>';
        $_SESSION[__CLASS__][$header][] = $message;
//        LogsTable::add([
//            'TITLE' => $header,
//            'TEXT' => $message,
//        ]);
    }

}
