<?php

namespace Project\Import\Import\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий.
 *
 * {@inheritdoc}
 */
class ImportListHelper extends AdminListHelper {

    protected static $model = '\Project\Import\Import\ImportTable';

    public function show() {
        LocalRedirect(ImportEditHelper::getUrl());
    }

}
