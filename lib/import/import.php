<?php

namespace Project\Import\Import;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

/**
 * Модель новостей.
 */
class ImportTable extends DataManager {

    public static function tableCreate() {
		self::tableDrop();
        static::getEntity()->getConnection()->query("CREATE TABLE " . self::getTableName() . " (
            ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            FILE INT,
            FILE_TYPE VARCHAR(255)
        );");
    }

    public static function tableDrop() {
        static::getEntity()->getConnection()->query("DROP TABLE IF EXISTS " . self::getTableName() . ";");
    }

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'd_project_import';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('FILE', array(
                'title' => 'Выберите файл'
            )),
            new Main\Entity\StringField('FILE_TYPE', array(
                'title' => 'Тип загрузки',
            ))
        );
    }
    
    public static function add(array $data) {

    }

}
