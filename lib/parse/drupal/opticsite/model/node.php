<?php

namespace Project\Import\Parse\Drupal\Opticsite\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class NodeTable extends DataManager {

    public static function getConnectionName() {
        return 'drupal';
    }

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'node';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('nid', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('vid'),
            new Main\Entity\StringField('type'),
            new Main\Entity\StringField('language'),
            new Main\Entity\StringField('title'),
            new Main\Entity\IntegerField('uid'),
            new Main\Entity\IntegerField('status'),
            new Main\Entity\StringField('created'),
            new Main\Entity\StringField('changed'),
            new Main\Entity\IntegerField('comment'),
            new Main\Entity\IntegerField('promote'),
        );
    }

}
