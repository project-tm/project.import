<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    Cutil,
    CFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    Bitrix\Main\Application,
    Project\Redirect\Redirect,
    Project\Import\View,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Comment {

    const LIMIT = 10000;

    static public function process($page) {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        $connect->queryExecute("SET NAMES 'utf8'");
        $connect->queryExecute('SET collation_connection = "utf8_general_ci"');

        $rsData = $connect->query('SELECT SQL_CALC_FOUND_ROWS * FROM comments WHERE nid="12" ORDER BY pid LIMIT ' . $start . ', ' . $limit);

        $count = $connect->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            self::importItem($arItem);
        }
        exit;
        return $pageIsNext;
    }

    public static function importItem($arData) {

        $arData['comment'] = Utility\Content::uploadSrcImage($arData['comment'], 'http://opticsite.ru');
        if ($arData['pid']) {
            $arFields = array(
                'IBLOCK_ID' => Config::COMMENT,
                'DETAIL_TEXT' => $arData['comment'],
                'DETAIL_TEXT_TYPE' => 'html',
            );
            $propFields = array(
                'ANSWER_TIME' => $arData['timestamp'],
            );
            try {
                $arItem = Utility\Iblock::searchByFilter(array(
                            'IBLOCK_ID' => Config::COMMENT,
                            'PROPERTY_OLD_NID' => $arData['pid']
                                ), $arFields, $propFields);
            } catch (Exception $exc) {
                pre($exc->getMessage());
                pre($arFields, $propFields);
            }

//            pre($arItem, $arFields, $propFields);
//            exit;
            Log::success('Добавленыm отзывы', $arItem['NAME']);
        } else {
//            pre($arData);

            $arFields = array(
                'DATE_ACTIVE_FROM' => ConvertTimeStamp($arData['timestamp'], 'FULL'),
                'TIMESTAMP_X' => ConvertTimeStamp($arData['timestamp'], 'FULL'),
                'DATE_CREATE' => ConvertTimeStamp($arData['timestamp'], 'FULL'),
                'IBLOCK_ID' => Config::COMMENT,
                'IBLOCK_SECTION_ID' => 0,
                'NAME' => $arData['subject'],
                'SORT' => '500',
                'ACTIVE' => (!$arData['status']) ? 'Y' : 'N',
                'CODE' => $arData['cid'],
//                'DETAIL_TEXT' => $arData['body'],
//                'DETAIL_TEXT_TYPE' => 'html',
                'PREVIEW_TEXT' => $arData['comment'],
                'PREVIEW_TEXT_TYPE' => 'html',
            );
            $propFields = array(
                'OLD_NID' => $arData['cid'],
                'OLD_VID' => $arData['pid'],
                'HOSTNAME' => $arData['hostname'],
                'NAME' => $arData['name'],
            );
            $arItem = Utility\Iblock::searchByFilter(array(
                        'IBLOCK_ID' => Config::COMMENT,
                        'PROPERTY_OLD_NID' => $propFields['OLD_NID']
                            ), $arFields, $propFields);
            Log::success('Добавленыm отзывы', $arItem['NAME']);
//            pre($arItem, $arFields, $propFields);
        }
    }

}
