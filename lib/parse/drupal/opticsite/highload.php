<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    Cutil,
    CFile,
    CDBResult,
    Bitrix\Main\Application,
    Project\Redirect\Redirect,
    Project\Import\View,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Highload {

    const LIMIT = 100;
    const TYPE = array(
        'Акционные значки' => 'Actions',
//        'Метки для товаров (новинки лидеры)' => 'Label',
        'Производитель' => 'Brands',
        'Цвет оправы' => 'Colors',
        'Пол (каталог)' => 'Sex',
        'Тип (каталог)' => array(
            38 => 'FramesType',
            39 => 'LensesType',
            4 => 'SunGlassesType',
            5 => 'SportGlassesType',
            7 => 'AccessoryType',
        ),
        /*
         * Оправы
         */
        'Модель/цвет (оправы)' => 'FramesModel',
        'Материал для оправ' => 'FramesMaterial',
        'Форма оправ' => 'FramesForm',
//        /*
//         * очковые линзы
//         */
        'Материал очковых линз (помощник)' => 'LensesMaterial',
        'Тип линз по фокусу (помощник)' => 'LensesTypeFocus',
//        'Астигматизм (помощник)' => 'LensesAstigmatism',
//        'Тип(индекс) линз(помощник)' => 'LensesIndex',
        'Светопропускание линз(помощник)' => 'LensesTransmission',
//        /*
//         * солнечнозащитные очки
//         */
        'Модель/цвет (солнцезащитные очки)' => 'SunGlassesModel',
        'Светофильтр' => 'Lightfilter',
//        /*
//         * Спортивные очки
//         */
        'Модель/цвет (спортивные очки)' => 'SportGlassesModel',
            /*
             * Аксессуары
             */
//        'Тип (каталог)' => 'Type',
    );

    static public function process($page) {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        $connect->queryExecute("SET NAMES 'utf8'");
        $connect->queryExecute('SET collation_connection = "utf8_general_ci"');

        $rsData = $connect->query('SELECT SQL_CALC_FOUND_ROWS t.*, v.name AS tName, h.parent  FROM term_data t LEFT JOIN vocabulary v ON(t.vid=v.vid) LEFT JOIN term_hierarchy h ON(h.tid=t.tid) WHERE v.name IN("' . implode('", "', array_keys(self::TYPE)) . '") LIMIT ' . $start . ', ' . $limit);

        $count = $connect->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            $arItem['img'] = $connect->query('SELECT path FROM term_image WHERE tid="' . (int) $arItem['tid'] . '"')
                    ->fetch();
            self::importItem($arItem);
        }
        return $pageIsNext;
    }

    public static function getTable($arData) {
        $tName = self::TYPE[$arData['tName']];
        if (is_array($tName)) {
            if (isset($tName[$arData['parent']])) {
                $tName = $tName[$arData['parent']];
            } else {
                throw new Exception('Не указана таблица: '. $tName);
            }
        }
        return $tName;
    }

    public static function importItem($arData) {
        static $arClass;
        try {
            $tName = self::getTable($arData);
        } catch (Exception $exc) {
            return;
        }

        if (empty($arClass[$tName])) {
            $arClass[$tName] = \Project\Core\Highload::get($tName);
        }
        switch ($tName) {
            case 'Actions':
            case 'Brands':
                break;

            default:
                $arData['name'] = strtolower($arData['name']);
                break;
        }
        $arData['description'] = Utility\Content::uploadSrcImage($arData['description'], 'http://opticsite.ru');
        $arFields = array(
            'UF_NAME' => trim($arData['name']),
        );

        $rsData = $arClass[$tName]::getList(array(
                    'select' => array('ID', 'UF_XML_ID', 'UF_NAME', 'UF_FILE', 'UF_OLD_ID', 'UF_FULL_DESCRIPTION'),
                    'filter' => array(
                        'LOGIC' => 'OR',
                        array('=UF_NAME' => $arFields['UF_NAME']),
                        array('=UF_OLD_ID' => $arFields['UF_OLD_ID']),
                    )
        ));
        $rsData = new CDBResult($rsData);
        if ($arItem = $rsData->Fetch()) {
            if (empty($arItem['UF_FILE']) and $arData['img']['path']) {
                if ($arFile = Utility\Image::upload('/sites/default/files/category_pictures/' . $arData['img']['path'], 'http://opticsite.ru/')) {
                    $arFields["UF_FILE"] = $arFile;
                }
            }
            if (!in_array($arData['tid'], $arItem["UF_OLD_ID"])) {
                $arFields["UF_OLD_ID"] = $arItem["UF_OLD_ID"];
                $arFields["UF_OLD_ID"][] = $arData['tid'];
            }
            if ($arData['description']) {

            }
            $arClass[$tName]::update($arItem['ID'], $arFields);
        } else {
            $arFields["UF_XML_ID"] = $arData['tid'];
            $arFields["UF_FULL_DESCRIPTION"] = $arData['description'];
            $arFields["UF_OLD_ID"] = array(
                $arData['tid']
            );
            $arFields["UF_FULL_DESCRIPTION"] = $arData['description'];
            if ($arData['img']['path']) {
                if ($arFile = Utility\Image::upload('/sites/default/files/category_pictures/' . $arData['img']['path'], 'http://opticsite.ru/')) {
                    $arFields["UF_FILE"] = $arFile;
                }
            }
            $arClass[$tName]::add($arFields);
        }
        Log::success('Обновлен: ' . $arData['tName'], $arFields['UF_NAME']);
    }

}
