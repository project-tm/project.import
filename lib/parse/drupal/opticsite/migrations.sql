TRUNCATE b_sale_order;
TRUNCATE b_sale_order_archive;
TRUNCATE b_sale_order_change;
TRUNCATE b_sale_order_delivery;
TRUNCATE b_sale_order_dlv_basket;
TRUNCATE b_sale_order_props_value;
TRUNCATE b_sale_basket;
TRUNCATE b_sale_basket_props;
TRUNCATE b_sale_order_dlv_basket;

ALTER TABLE b_sale_order AUTO_INCREMENT = 5000;