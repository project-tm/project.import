<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem;
use Exception,
    CDBResult,
    CUser,
    CIBlockElement,
    CSaleStatus,
    Bitrix\Sale,
    Bitrix\Sale\Internals\OrderTable,
    Bitrix\Main\Application,
    Project\Import\View,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Order {

    const LIMIT = 50;

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        $connect->queryExecute("SET NAMES 'utf8'");
        $connect->queryExecute('SET collation_connection = "utf8_general_ci"');

//        $arUser = array();
//        $rsData = CUser::GetList(($by = "personal_country"), ($order = "desc"), false, array('ID', 'XML_ID'));
//        while ($arItem = $rsData->Fetch()) {
//            $arUser[$arItem['XML_ID']] = $arItem['ID'];
//        }

        $arFilter = array(
            'PROPERTY_OLD_NID',
            'IBLOCK_ID' => array(Config::CATALOG_ID, Config::OFFERS_ID),
        );
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID', 'PROPERTY_OLD_NID'));
        $arProduct = array();
        while ($arItem = $res->fetch()) {
            $arProduct[$arItem['PROPERTY_OLD_NID_VALUE']] = $arItem['ID'];
        }

        $arStatus2 = array();
        $rsData = CSaleStatus::getList();
        while ($arItem = $rsData->Fetch()) {
            if (empty($arItem['SORT'])) {
                $arItem['SORT'] = 1;
            }
            $arStatus2[$arItem['SORT']] = $arItem['ID'];
        }

        $arStatus = array();
        $rsData = $connect->query('SELECT * FROM uc_order_statuses');
        while ($arItem = $rsData->Fetch()) {
            if (empty($arItem['weight'])) {
                $arItem['weight'] = 1;
            }
            $arStatus[$arItem['order_status_id']] = $arStatus2[$arItem['weight']];
        }


        $arStatus = array();
        $rsData = $connect->query('SELECT * FROM uc_order_statuses');
        while ($arItem = $rsData->Fetch()) {
            if (empty($arItem['weight'])) {
                $arItem['weight'] = 1;
            }
            $arStatus[$arItem['order_status_id']] = $arStatus2[$arItem['weight']];
        }

//        $res = explode(';', 'TRUNCATE b_sale_order; TRUNCATE b_sale_order_archive; TRUNCATE b_sale_order_change; TRUNCATE b_sale_order_delivery; TRUNCATE b_sale_order_dlv_basket; TRUNCATE b_sale_order_props_value; TRUNCATE b_sale_basket; TRUNCATE b_sale_basket_props; TRUNCATE b_sale_order_dlv_basket;');
//        foreach ($res as $value) {
//            if ($value) {
//                Application::getConnection()->query($value);
//            }
//        }

//        $rsData = $connect->query('SELECT SQL_CALC_FOUND_ROWS * FROM uc_orders WHERE order_id=3205 LIMIT ' . $start . ', ' . $limit);
        $rsData = $connect->query('SELECT SQL_CALC_FOUND_ROWS * FROM uc_orders LIMIT ' . $start . ', ' . $limit);

        $count = $connect->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            $arItem['order_status'] = $arStatus[$arItem['order_status']];
            $arItem['USER_ID'] = $arUser[$arItem['order_status']];
            $res = $connect->query('SELECT u.* FROM uc_order_admin_comments u WHERE u.order_id="' . (int) $arItem['order_id'] . '" ORDER BY created DESC');
            while ($arImg = $res->Fetch()) {
                $arItem['uc_order_admin_comments'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_order_comments u WHERE u.order_id="' . (int) $arItem['order_id'] . '" ORDER BY created DESC');
            while ($arImg = $res->Fetch()) {
                $arItem['uc_order_comments'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_order_log u WHERE u.order_id="' . (int) $arItem['order_id'] . '" ORDER BY created DESC');
            while ($arImg = $res->Fetch()) {
                $arItem['uc_order_log'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_order_quotes u WHERE u.order_id="' . (int) $arItem['order_id'] . '"');
            while ($arImg = $res->Fetch()) {
                $arItem['uc_order_quotes'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_order_products u WHERE u.order_id="' . (int) $arItem['order_id'] . '"');
            while ($arImg = $res->Fetch()) {
                $arImg['PROPDUCT_ID'] = $arProduct[$arImg['order_product_id']] ?: '';
                $arItem['uc_order_products'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_order_line_items u WHERE u.order_id="' . (int) $arItem['order_id'] . '"');
            while ($arImg = $res->Fetch()) {
                $arImg['price'] = $arImg['amount'];
                $arItem['uc_order_products'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_payment_receipts u WHERE u.order_id="' . (int) $arItem['order_id'] . '"');
            while ($arImg = $res->Fetch()) {
                $arItem['PAYMEND'] = $arItem['PAYMEND'] ?: 0 + $arImg['amount'];
                $arItem['uc_payment_receipts'][] = $arImg;
            }
            $res = $connect->query('SELECT u.* FROM uc_extra_fields_values u WHERE u.element_id="' . (int) $arItem['order_id'] . '"');
            while ($arImg = $res->Fetch()) {
                if ($arImg['value']) {
                    $arItem['uc_extra_fields_values'][] = $arImg['value'];
                }
            }
            self::importItem($arItem);
        }
//        exit;
        return $pageIsNext;
    }

    public static function importItem($arData) {
        if (empty($arData['primary_email'])) {
            $arData['primary_email'] = $arData['uc_extra_fields_values'][0];
        }

        $filter = Array(
            "EMAIL" => $arData['primary_email']
        );
        $res = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array('ID'));
        if ($arItem = $res->Fetch()) {
            $arData['USER_ID'] = $arItem['ID'];
        } else {
            $user = new CUser;
            $pass = "DRUPAL-AbC" . rand(300, 1999);
            $arFields = Array(
                "LAST_NAME" => $arData["delivery_first_name"],
                "LAST_NAME" => $arData["delivery_last_name"],
                "EMAIL" => $arData["primary_email"],
                "LOGIN" => $arData["primary_email"],
                'LAST_LOGIN' => ConvertTimeStamp($arData['created'], 'FULL'),
                'TIMESTAMP_X' => ConvertTimeStamp($arData['created'], 'FULL'),
                'DATE_REGISTER' => ConvertTimeStamp($arData['created'], 'FULL'),
                "LID" => SITE_ID,
                "ACTIVE" => $arData['status'] ? "Y" : 'N',
                "XML_ID" => $arData["uid"],
                "PASSWORD" => $pass,
                "CONFIRM_PASSWORD" => $pass,
            );
            if (!$arData['USER_ID'] = $user->Add($arFields)) {
                $arFields['EMAIL'] = 'order' . $arFields['XML_ID'] . '@opticsite.ru';
                $filter = Array(
                    "EMAIL" => $arFields['EMAIL']
                );
                $res = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array('ID'));
                if ($arItem = $res->Fetch()) {
                    $arData['USER_ID'] = $arItem['ID'];
                } else {
                    if (!$arData['USER_ID'] = $user->Add($arFields)) {
                        throw new Exception($user->LAST_ERROR);
                        return;
                    }
                }
            }
//            preExit($arFields);
        }

        $comment = array();
        foreach ($arData['uc_order_admin_comments'] as $value) {
            $comment[$value['created']][] = date('Y-m-d H:i:s', $value['created']) . ': ' . trim(strip_tags($value['message']));
        }
        foreach ($arData['uc_order_comments'] as $value) {
            $comment[$value['created']][] = date('Y-m-d H:i:s', $value['created']) . ': ' . trim(strip_tags($value['message']));
        }
        foreach ($arData['uc_order_log'] as $value) {
            $comment[$value['created']][] = date('Y-m-d H:i:s', $value['created']) . ': ' . trim(strip_tags($value['changes']));
        }
        ksort($comment);
        $comment2 = array();
        foreach ($comment as $value) {
            foreach ($value as $value2) {
                $comment2[] = $value2;
            }
        }



        $arProducts = array();
        foreach ($arData['uc_order_products'] as $key => $value) {
            $comment2[] = '';
            if (empty($key)) {
                $comment2[] = 'Товары:';
            }

            $res = array();
            foreach (array('order_product_id', 'manufacturer', 'model', 'title') as $key2) {
                if ($value[$key2]) {
                    $res[] = $value[$key2];
                }
            }
            $item = array(
                'NAME' => count($res) > 1 ? '[' . implode('][', $res) . ']' : implode('', $res),
                'QUANTITY' => $value['qty'] ?: 1,
                'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'PRICE' => $value['price'] ?: 0,
            );

            $data = unserialize($value['data']);
            $keys = array(
                'comment' => 'Комментарий',
                'selectSPH' => 'Оптическая сила (SPH)',
                'selectCYL' => 'Цилиндр (CYL)',
                'add' => 'Аддидация (ADD)',
                'selectAX' => 'Ось (AX)',
                'diametr' => 'Диаметр (мм)',
                'field_actions' => 'Акция',
            );
            foreach ($keys as $key2 => $value2) {
                if (!empty($data[$key2])) {
                    $item['PROPS'][$key2] = array(
                        'NAME' => $keys[$key2] ?: $key2,
                        'VALUE' => $data[$key2]
                    );
                }
            }
            $comment2[] = $item['QUANTITY'] . ' x ' . $item['NAME'] . ' (' . $item['PRICE'] . 'руб.)';
            if (isset($item['PROPS'])) {
                foreach ($item['PROPS'] as $key2 => $value2) {
                    $comment2[] = $value2['NAME'] . ': ' . $value2['VALUE'];
                }
            }

            $arProducts[$key] = $item;
//            pre($item);
        }

//        preExit($arData, $comment2, $arProducts);
//        preExit($arProducts);


        $connect = Application::getConnection();
        $res = $connect->query('SELECT * FROM b_sale_order WHERE id="' . (int) $arData['order_id'] . '"');
        if (! $res->fetch()) {
//            preExit($arData);
            $arData['created'] = date('Y-m-d H:i:s', $arData['created']);
            $arData['modified'] = date('Y-m-d H:i:s', $arData['modified']);

            $b_sale_order = array(
                'ID' => $arData['order_id'], 'LID' => 's1', 'PERSON_TYPE_ID' => '1', 'PAYED' => $arData['PAYMEND'] ? 'N' : 'N', 'DATE_PAYED' => NULL, 'EMP_PAYED_ID' => NULL,
                'CANCELED' => 'N', 'DATE_CANCELED' => NULL, 'EMP_CANCELED_ID' => NULL, 'REASON_CANCELED' => NULL,
                'STATUS_ID' => $arData['order_status'], 'DATE_STATUS' => $arData['modified'], 'EMP_STATUS_ID' => $arData['USER_ID'],
                'PRICE_DELIVERY' => '0.0000',
//                'PRICE_PAYMENT' => '0.0000',
                'ALLOW_DELIVERY' => 'N', 'DATE_ALLOW_DELIVERY' => NULL, 'EMP_ALLOW_DELIVERY_ID' => NULL, 'DEDUCTED' => 'N', 'DATE_DEDUCTED' => NULL, 'EMP_DEDUCTED_ID' => NULL, 'REASON_UNDO_DEDUCTED' => NULL, 'MARKED' => 'N', 'DATE_MARKED' => NULL, 'EMP_MARKED_ID' => NULL, 'REASON_MARKED' => NULL, 'RESERVED' => 'N',
                'PRICE' => $arData['order_total'], 'CURRENCY' => 'RUB', 'DISCOUNT_VALUE' => '0.0000', 'USER_ID' => $arData['USER_ID'], 'PAY_SYSTEM_ID' => '7', 'DELIVERY_ID' => '2',
                'DATE_INSERT' => $arData['modified'], 'DATE_UPDATE' => $arData['modified'], 'USER_DESCRIPTION' => '', 'ADDITIONAL_INFO' => NULL,
//                'PS_STATUS' => NULL, 'PS_STATUS_CODE' => NULL, 'PS_STATUS_DESCRIPTION' => NULL, 'PS_STATUS_MESSAGE' => NULL, 'PS_SUM' => NULL, 'PS_CURRENCY' => NULL, 'PS_RESPONSE_DATE' => NULL,
                'COMMENTS' => '', 'TAX_VALUE' => '0.00', 'STAT_GID' => NULL, 'SUM_PAID' => '0.00', 'RECURRING_ID' => NULL, 'PAY_VOUCHER_NUM' => NULL, 'PAY_VOUCHER_DATE' => NULL,
                'LOCKED_BY' => $arData['USER_ID'], 'DATE_LOCK' => '2017-07-07 17:03:01', 'RECOUNT_FLAG' => 'Y', 'AFFILIATE_ID' => NULL, 'DELIVERY_DOC_NUM' => NULL, 'DELIVERY_DOC_DATE' => NULL, 'UPDATED_1C' => 'N', 'STORE_ID' => NULL, 'ORDER_TOPIC' => NULL,
                'CREATED_BY' => $arData['USER_ID'], 'RESPONSIBLE_ID' => NULL, 'DATE_PAY_BEFORE' => NULL, 'DATE_BILL' => NULL,
                'ACCOUNT_NUMBER' => $arData['order_id'], 'TRACKING_NUMBER' => NULL, 'XML_ID' => NULL, 'ID_1C' => NULL, 'VERSION_1C' => NULL, 'VERSION' => '0', 'EXTERNAL_ORDER' => 'N', 'BX_USER_ID' => '', 'COMPANY_ID' => NULL, 'IS_RECURRING' => 'N', 'RUNNING' => 'N'
            );
//            preExit($arData);
            $connect->query("TRUNCATE TABLE `b_sale_order_processing`");
            $connect->query("REPLACE `b_sale_order` (`" . implode('`, `', array_keys($b_sale_order)) . "`) VALUES('" . implode("', '", array_map(function($res) {
                                return addslashes($res);
                            }, $b_sale_order)) . "')");

//                    exit;
            $order = Sale\Order::load($arData['order_id']);
            $order->setPersonTypeId(1);

            $sep = ' ';
            $func = function() use($arData, $sep) {
                $res = array();
                foreach (func_get_args() as $key) {
                    if ($arData[$key]) {
                        $res[] = $arData[$key];
                    }
                }
                return implode($sep, $res);
            };

            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                        'select' => array(
                            'ID',
                            'NAME_RU' => 'NAME.NAME'
                        ),
                        'filter' => array(
                            '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
                            'NAME_RU' => $arData['delivery_city']
                        )
            ));
            $arLocation = $res->fetch();

            $propertyCollection = $order->getPropertyCollection();
            $propertyCollection->getItemByOrderPropertyId(1)->setValue($func('delivery_first_name', 'delivery_last_name'));
            $propertyCollection->getItemByOrderPropertyId(2)->setValue($arData['delivery_phone']);
            $propertyCollection->getItemByOrderPropertyId(3)->setValue($arData['primary_email']);
            $propertyCollection->getItemByOrderPropertyId(4)->setValue($arLocation['ID']);
//            pre($arLocation['ID']);
            $propertyCollection->getItemByOrderPropertyId(5)->setValue($arData['delivery_postal_code']);
            $sep = ', ';
            $propertyCollection->getItemByOrderPropertyId(6)->setValue($func('delivery_city', 'delivery_postal_code', 'delivery_street1'));

            $order->setField('COMMENTS', implode(PHP_EOL, $comment2));


            $basket = $order->getBasket();

            foreach ($arData['uc_order_products'] as $key => $value) {
                $item = $basket->createItem($value['PRODUCT_ID'] ? 'catalog' : 'CUSTOM', $value['PRODUCT_ID'] ?: $key);
                $res = array();
                foreach (array('order_product_id', 'manufacturer', 'model', 'title') as $key2) {
                    if ($value[$key2]) {
                        $res[] = $value[$key2];
                    }
                }
//                    pre(array(
//                        'NAME' => count($res) > 1 ? '[' . implode('][', $res) . ']' : implode('', $res),
//                        'QUANTITY' => $value['qty'] ?: 1,
//                        'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
//                        'LID' => 's1',
//                        'PRICE' => $value['price'] ?: 0,
//                        'CUSTOM_PRICE' => 'Y',
//                    ));
                $arProduct = $arProducts[$key];
//                    pre($arProduct);
                $item->setFields(array(
                    'NAME' => $arProduct['NAME'],
                    'QUANTITY' => $arProduct['QUANTITY'],
                    'CURRENCY' => $arProduct['CURRENCY'],
                    'LID' => 's1',
                    'PRICE' => $arProduct['PRICE'],
                    'CUSTOM_PRICE' => 'Y',
                ));
                if (!empty($arProduct['PROPS'])) {
                    $basketPropertyCollection = $item->getPropertyCollection();
                    foreach ($arProduct['PROPS'] as $key2 => $value2) {
                        $basketPropertyCollection->setProperty(array(
                            array(
                                'NAME' => $value2['NAME'],
                                'CODE' => strtoupper($key2),
                                'VALUE' => $value2['VALUE'],
                                'SORT' => 100,
                            ),
                        ));
                    }

//                    pre($basketPropertyCollection->getPropertyValues());
//                    $basketPropertyCollection->save();
                    $item->save();
                    $basket->save();
                }
            }

//            preExit($arProducts);

            if ($arData['PAYMEND'] and $arData['PAYMEND'] == $arData['order_total']) {
                $paymentCollection = $order->getPaymentCollection();
                $payment = $paymentCollection->createItem();
                $paySystemService = PaySystem\Manager::getObjectById(7);
                $payment->setFields(array(
                    'PAID' => 'Y',
                    'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
                    'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
                ));
            }
            $order->doFinalAction(true);
            $order->save();
        } else {
//            preExit($arData);
//            $order->setUserId($arData['USER_ID']);
            $order = Sale\Order::load($arData['order_id']);

            $order->setField('COMMENTS', implode(PHP_EOL, $comment2));

            $order->save();
//            $order = Sale\Order::load($arData['order_id']);
        }



//        exit;


//        preExit($arData, $arFields);
        Log::success('Обновлен: ', $arData['order_id']);
    }

}
