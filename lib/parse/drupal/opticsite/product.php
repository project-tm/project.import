<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    Cutil,
    CFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    Bitrix\Main\Application,
    Project\Redirect\Redirect,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Product {

    const LIMIT = 50;
    const TYPE = array(
        /*
         * услуги
         */
        'making_optic' => 6,
        'services_oth' => 6,
        /*
         * аксессуары
         */
        'access_glasses' => 5,
        /*
         * Очки
         */
        'sport_glasses' => 4,
        'sun_glasses' => 3,
        /*
         * Линзы очковые
         */
        'och_linzu' => 2,
        /*
         * Оправы
         */
        'glasses' => 1,
    );

    static protected function getType() {
        return array_keys(self::TYPE);
    }

    use Traits\Node;

    public static function report($arItem, $propFields) {
        if (!empty($propFields['COLOR_TEXT'])) {
            Utility\Report::add('color', 'http://opticsite.dev-tm.ru/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=' . $arItem['IBLOCK_ID'] . '&type=catalog&ID=' . $arItem['ID']);
        }
        if (empty($arItem['ID'])) {
            preExit($arItem);
        }
    }

    public static function importProduct($arData) {
        $connect = self::getConnection();
        if (empty($arData['url']['dst'])) {
            $arData['url']['dst'] = 'node/'. $arData['nid'];
//            preExit($arData);
//            return;
        }

        $arData['product'] = $connect->query('SELECT u.* FROM uc_products u  WHERE u.nid="' . (int) $arData['nid'] . '" AND u.vid="' . (int) $arData['vid'] . '"')
                ->fetch();
        $arData['body'] = Utility\Content::uploadSrcImage($arData['body'], 'http://opticsite.ru');
        $arData['meta'] = Utility\Content::getMeta('http://opticsite.ru/node/' . (int) $arData['nid']);

        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'TIMESTAMP_X' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'DATE_CREATE' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'IBLOCK_ID' => Config::CATALOG_ID,
            'IBLOCK_SECTION_ID' => self::TYPE[$arData['type']],
            'NAME' => $arData['title'],
            'SORT' => '500',
            'ACTIVE' => $arData['status'] ? 'Y' : 'N',
            'CODE' => Cutil::translit(empty($arData['url']['dst']) ? $arData['title'] : str_replace(array('service/'), '', $arData['url']['dst']), "ru", array("replace_space" => "-", "replace_other" => "-")),
            'DETAIL_TEXT' => $arData['body'],
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => $arData['teaser'],
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'TEST' => 56,
            'MODEL' => $arData['product']['model'] ?: '',
            'OLD_NID' => $arData['nid'],
            'OLD_VID' => $arData['vid'],
            'TITLE' => $arData['meta']['title'] ?: '',
            'KEYWORDS' => $arData['meta']['keywords'] ?: '',
            'META_DESCRIPTION' => $arData['meta']['description'] ?: '',
        );
        $arParam = self::importParam($arData, $propFields);
//        pre($arParam, $propFields);

        $typeRedirect = 'CATALOG';
        $isSearch = $arOffers = false;
        switch ($arData['type']) {
            case 'glasses':
                if ($propFields['FRAMESMODEL']) {
                    $isSearch = true;
                    $arFilter = array(
                        "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                        'PROPERTY_FRAMESMODEL' => $propFields['FRAMESMODEL']
                    );
                }
                break;

            case 'sport_glasses':
                if ($propFields['SPORTGLASSESMODEL']) {
                    $isSearch = true;
                    $arFilter = array(
                        "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                        'PROPERTY_SPORTGLASSESMODEL' => $propFields['SPORTGLASSESMODEL']
                    );
                }
                break;

            case 'sun_glasses':
                if ($propFields['SUNGLASSESMODEL']) {
                    $isSearch = true;
                    $arFilter = array(
                        "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                        'PROPERTY_SUNGLASSESMODEL' => $propFields['SUNGLASSESMODEL']
                    );
                }
                break;

            default:
                break;
        }
//        preExit($propFields);
//        preExit($arData['type'], $arFilter, $propFields);
        if (isset($arFilter)) {
            $arItem = Utility\Catalog::searchByFilter($arFilter, $arFields, $propFields, false);
            $arFields['DETAIL_TEXT'] = $arFields['PREVIEW_TEXT'] = '';
            if ($arItem['PROPERTY_OLD_NID_VALUE'] == $propFields['OLD_NID'] and $arItem['PROPERTY_OLD_VID_VALUE'] == $propFields['OLD_VID']) {
                $arFields['IBLOCK_ID'] = Config::OFFERS_ID;
                $propFields['CML2_LINK'] = $arItem['ID'];
                $arOffers = Utility\Catalog::searchByName($arFields, $propFields);
                self::report($arItem, $propFields);
                self::report($arOffers, $propFields);
            } else {
                $arFields['IBLOCK_ID'] = Config::OFFERS_ID;
//                        pre($arFields['IBLOCK_ID']);
                $propFields['CML2_LINK'] = $arItem['ID'];
                $typeRedirect = 'OFFERS';
                $arItem = Utility\Catalog::searchByName($arFields, $propFields);
                self::report($arItem, $propFields);
            }
//            preExit($arFields);
        }
        if (empty($isSearch)) {
            $arItem = Utility\Catalog::searchByName($arFields, $propFields);
            self::report($arItem, $propFields);
        } else {
            Utility\Catalog::update($arItem, $arFields, $propFields);
        }
//        preExit($arItem);
        if (empty($arItem)) {
            Log::error('Не найдены товары', $name);
            return;
        }
        if ($arData['url']['dst']) {
            Redirect::add('/' . $arData['url']['dst'], $typeRedirect, $arItem['ID']);
//            pre('/' . $arData['url']['dst'], 'CATALOG', $arItem['ID']);
        }
        Redirect::add('/node/' . $arData['nid'], $typeRedirect, $arItem['ID']);

//        pre($propFields, $arItem);
//        pre('/node/' . $arData['nid'], 'CATALOG', $arItem['ID']);
//        preExit();
        $arFields = $propFields = $arOffersFields = $propOffersFields = array();
        $arFields['DETAIL_TEXT'] = $arData['body'];
        $arFields['DETAIL_TEXT_TYPE'] = 'html';
        $arFields['PREVIEW_TEXT'] = $arData['teaser'];
        $arFields['PREVIEW_TEXT_TYPE'] = 'html';
        if ($arData['img']) {
            $img = array_shift($arData['img']);
            if (!empty($img)) {
                if ($arFile = Utility\Image::upload($img, 'http://opticsite.ru/')) {
                    if (empty($arItem['DETAIL_PICTURE'])) {
                        $arFields["DETAIL_PICTURE"] = $arFile;
                    }
                    if ($arOffers and empty($arOffers['DETAIL_PICTURE'])) {
                        $arOffersFields["DETAIL_PICTURE"] = $arFile;
                    }
                }
            }
            foreach ($arData['img'] as $img) {
                if ($arFile = Utility\Image::upload($img, 'http://opticsite.ru/')) {
                    if (empty($arItem['PROPERTY_MORE_PHOTO_VALUE'])) {
                        $propFields["MORE_PHOTO"][] = $arFile;
                    }
                    if ($arOffers and empty($arOffers['PROPERTY_MORE_PHOTO_VALUE'])) {
                        $propOffersFields["MORE_PHOTO"][] = $arFile;
                    }
                }
            }
        }

//        preExit($arFields, $arOffersFields);

        Utility\Catalog::update($arItem, $arFields, $propFields);
        Utility\Catalog::saveCatalog($arItem);
        if ($arData['product'] and $arData['product']['sell_price']) {
            Utility\Catalog::savePrice($arItem, $arData['product']['sell_price'], 'RUB');
        }
        if ($arOffers) {
            $arOffersFields['IBLOCK_ID'] = Config::OFFERS_ID;
            Utility\Catalog::update($arOffers, $arOffersFields, $propOffersFields);
            Utility\Catalog::saveCatalog($arOffers);
            if ($arData['product'] and $arData['product']['sell_price']) {
                Utility\Catalog::savePrice($arOffers, $arData['product']['sell_price'], 'RUB');
            }
        }

        switch ($arData['type']) {
            case 'och_linzu':
                self::importOffersLenses($arData, $arItem, $arParam, $propFields);
                break;

            default:
                break;
        }

//        preExit($arItem);
//        preExit($arItem, $arFields, $propFields);
        Log::success('Добавлены товары', $arItem['NAME']);
    }

    static protected function importParam($arData, &$propFields) {
        $connect = self::getConnection();
        $arParam = array();
        $param = array(
            'STORE_STATUS' => 'status',
            'Sex' => 'sex',
            'Brands' => 'brand',
            'CYL_MIN' => 'formcyl_start',
            'CYL_MAX' => 'formcyl_end',
            'ADD_MIN' => 'formadd_start',
            'ADD_MAX' => 'formadd_end',
//            'SPH_MIN' => 'formsph_start',
//            'SPH_MAX' => 'formsph_end',
//            'SPH_STEP' => 'formsph_step',
//            'D' => 'formd',
            'COLOR_TEXT' => 'color_description',
            'FramesMaterial' => 'material_o',
            'A_0' => 'a',
            'B_0' => 'b',
            'C_0' => 'c',
            'LIGHT_FILTER' => 'color',
            'LensesTransmission' => 'light_transm',
//            'SportGlassesModel' => 'color_sportglasses',
//            'light_l' => 'index',
//            'IMAGE' => array(
//                'image' => 'fid'
//            ),
        );
        foreach ($param as $key => $value) {
            if (is_array($value)) {
                list($value, $field) = each($value);
            } else {
                $field = 'value';
            }
            $res = $connect->query('SELECT field_' . $value . '_' . $field . ' AS value FROM content_field_' . $value . '  WHERE nid="' . (int) $arData['nid'] . '" AND vid="' . (int) $arData['vid'] . '"');
            while ($item = $res->Fetch()) {
//                if ($item['value']) {
                $arParam[$key][] = trim(Utility\Content::parseFloat($item['value']));
//                }
            }
        }
        $param = array(
            'SPH_MIN' => 'formsph_start',
            'SPH_MAX' => 'formsph_end',
            'SPH_STEP' => 'formsph_step',
            'D' => 'formd',
        );
        foreach ($param as $key => $value) {
            if (is_array($value)) {
                list($value, $field) = each($value);
            } else {
                $field = 'value';
            }
            $res = $connect->query('SELECT delta, field_' . $value . '_' . $field . ' AS value FROM content_field_' . $value . '  WHERE nid="' . (int) $arData['nid'] . '" AND vid="' . (int) $arData['vid'] . '" ORDER BY delta');
            while ($item = $res->Fetch()) {
//                if ($item['value']) {
                $arParam[$key][$item['delta']] = $item['value'];
//                }
            }
        }
        $res = $connect->query('SELECT n.tid, t.name, v.name AS tName, h.parent FROM term_node n LEFT JOIN term_data t ON (t.tid=n.tid) LEFT JOIN vocabulary v ON(t.vid=v.vid) LEFT JOIN term_hierarchy h ON(h.tid=t.tid) WHERE n.nid="' . (int) $arData['nid'] . '" AND n.vid="' . (int) $arData['vid'] . '" GROUP BY tid');
        while ($item = $res->Fetch()) {
            $table = Highload::getTable($item);
//            pre($table);
            $arParam[$table][] = $item['tid']; //Utility\Highload::searchOldId($table, $item['tid']);
//            pre($arParam[$table]);
        }

        foreach (array(
    'Sex',
    'Brands',
    'Actions',
        ) as $table) {
            if (isset($arParam[$table])) {
                foreach ($arParam[$table] as $key => $value) {
                    $arParam[$table][$key] = Utility\Highload::searchOldId($table, $value);
                }
            }
        }


        foreach (array(
    'A_0' => 'A_0',
    'B_0' => 'B_0',
    'C_0' => 'C_0',
    'COLOR_TEXT' => 'COLOR_TEXT',
    'ACTIONS' => 'Actions',
    'BRANDS' => 'Brands',
    'BRAND' => 'Brands',
    'COLOR' => 'Colors',
    'SPORTGLASSESMODEL' => 'SportGlassesModel',
    'SUNGLASSESMODEL' => 'SunGlassesModel',
    'FRAMESMODEL' => 'FramesModel',
    'FRAMES_FORM' => 'FramesForm',
    'LENSES_INDEX' => 'LensesIndex',
//    'LENSES_ASTIGMATISM' => 'LensesAstigmatism',
    'LIGHT_FILTER' => 'Lightfilter',
    'CYL_MIN' => 'CYL_MIN',
    'CYL_MAX' => 'CYL_MAX',
    'ADD_MIN' => 'ADD_MIN',
    'ADD_MAX' => 'ADD_MAX',
    'STORE_STATUS' => 'STORE_STATUS',
        ) as $key => $param) {
            if ($arParam[$param]) {
                $propFields[$key] = $arParam[$param][0];
            }
        }
        if (!empty($propFields['COLOR_TEXT'])) {
            $propFields['COLOR_TEXT'] = trim(html_entity_decode(strip_tags($propFields['COLOR_TEXT'])));
        }

        foreach (array(
    'LIGHT_FILTER' => 'LIGHT_FILTER',
//    'ACTIONS' => 'Label',
    'SEX' => 'Sex',
//    'COLORS' => 'Colors',
    'LENSES_MATERIAL' => 'LensesMaterial',
    'FRAMES_TYPE' => 'FramesType',
    'SUN_GLASSES_TYPE' => 'SunGlassesType',
    'SUN_GLASSES_MATERIAL' => 'SunGlassesType',
    'SPORT_GLASSES_TYPE' => 'SportGlassesType',
    'ACCESSORY_TYPE' => 'AccessoryType',
    'FRAMES_MATERIAL' => 'FramesMaterial',
//    'LENSES_TYPE' => 'LensesType',
    'LENSES_TYPE_FOCUS' => 'LensesTypeFocus',
    'LENSES_TRANSMISSION' => 'LensesTransmission',
        ) as $key => $param) {
            if ($arParam[$param]) {
                $propFields[$key] = $arParam[$param];
            }
        }

//        preExit($arParam, $propFields);

        $arData['type_data'] = $connect->query('SELECT c.* FROM content_type_' . $arData['type'] . ' c WHERE c.nid="' . (int) $arData['nid'] . '" AND c.vid="' . (int) $arData['vid'] . '"')
                ->fetch();
        switch ($arData['type']) {
            case 'och_linzu':
                if (!empty($arData['type_data']['field_index_value'])) {
//                    $propFields['LENSES_INDEX'] = Utility\Highload::search('LensesIndex', $arData['type_data']['field_index_value'], $arData['type_data']['field_index_value']);
                    $propFields['LENSES_INDEX'] = $arData['type_data']['field_index_value'];
                }
                break;

            case 'glasses':
                if (!empty($arData['type_data']['field_color_glasses_value'])) {
                    $propFields['FRAMESMODEL'] = Utility\Highload::search('FramesModel', $arData['type_data']['field_color_glasses_value'], $arData['type_data']['field_color_glasses_value']);
                }
                break;

            case 'sport_glasses':
                if (!empty($arData['type_data']['field_color_sportglasses_value'])) {
                    $propFields['SPORTGLASSESMODEL'] = Utility\Highload::search('SportGlassesModel', $arData['type_data']['field_color_sportglasses_value'], $arData['type_data']['field_color_sportglasses_value']);
                }
                break;

            case 'sun_glasses':
                if (!empty($arData['type_data']['field_color_sunglasses_value'])) {
                    $propFields['SUNGLASSESMODEL'] = Utility\Highload::search('SunGlassesModel', $arData['type_data']['field_color_sunglasses_value'], $arData['type_data']['field_color_sunglasses_value']);
                }
                break;

            default:
                break;
        }
        return $arParam;
    }

    static protected function importOffersLenses($arData, $arItem, $arParam, $propFields) {
        if (isset($arParam['SPH_MIN'])) {
            pre($arParam['SPH_MIN'], $arParam['SPH_STEP'], $arParam['SPH_MAX'], $arParam['D']);
            foreach ($arParam['SPH_MIN'] as $key => $value) {
                $arFields = array(
                    'IBLOCK_ID' => Config::OFFERS_ID,
                    'NAME' => $arItem['NAME'],
                );
                $propFields = array(
                    'CML2_LINK' => $arItem['ID'],
                    'COLOR' => $propFields['COLORS'],
                    'SPH_MIN' => $arParam['SPH_MIN'][$key] ?: '0',
                    'SPH_MAX' => $arParam['SPH_MAX'][$key] ?: '0',
                    'SPH_STEP' => $arParam['SPH_STEP'][$key] ?: '0',
                    'D' => $arParam['D'][$key] ?: '',
                );
                $propFields['D'] = trim(str_replace(array('мм', 'mm'), '', $propFields['D']));
                $propFields = array_map('trim', $propFields);
                $arFilter = array(
                    'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                );
                foreach ($propFields as $key => $value) {
                    $arFilter['=PROPERTY_' . $key] = $value;
                }
                $arOffers = Utility\Catalog::searchByFilter($arFilter, $arFields, $propFields);
                Utility\Catalog::saveCatalog($arOffers);
                if ($arData['product'] and $arData['product']['sell_price']) {
                    Utility\Catalog::savePrice($arOffers, $arData['product']['sell_price'], 'RUB');
                }
                pre($arData['ID'], $propFields);
            }
        }
//        preExit($arParam);
    }

}
