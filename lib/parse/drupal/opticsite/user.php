<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    CDBResult,
    CUser,
    Bitrix\Main\Application,
    Project\Import\View,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class User {

    const LIMIT = 100;

    static public function process($page) {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        $connect->queryExecute("SET NAMES 'utf8'");
        $connect->queryExecute('SET collation_connection = "utf8_general_ci"');

        $rsData = $connect->query('SELECT SQL_CALC_FOUND_ROWS * FROM users LIMIT ' . $start . ', ' . $limit);

        $count = $connect->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            self::importItem($arItem);
        }
        return $pageIsNext;
    }

    public static function importItem($arData) {
        if (empty($arData['mail'])) {
            return;
        }

        $user = new CUser;
        $arFields = Array(
            "NAME" => $arData["name"],
            "EMAIL" => $arData["mail"],
            "LOGIN" => $arData["name"],
            'LAST_LOGIN' => ConvertTimeStamp($arData['login'], 'FULL'),
            'TIMESTAMP_X' => ConvertTimeStamp($arData['login'], 'FULL'),
            'DATE_REGISTER' => ConvertTimeStamp($arData['created'], 'FULL'),
            "LID" => SITE_ID,
            "ACTIVE" => $arData['status'] ? "Y" : 'N',
            "XML_ID" => $arData["uid"],
        );
//        pre($arData, $arFields);
        $filter = Array(
            "LOGIN" => $arFields['LOGIN']
        );
        $res = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array('ID'));
        if ($arItem = $res->Fetch()) {
            $arFields['UF_PASSWORD'] = $arData['pass'];
            $user->update($arItem["ID"], $arFields);
//            pre($arItem["ID"], $arFields);
        } else {
            $pass = "DRUPAL-AbC" . rand(300, 1999);
            $arFields['UF_PASSWORD'] = $arData['pass'];
            $arFields['PASSWORD'] = $pass;
            $arFields['CONFIRM_PASSWORD'] = $pass;
            if (!$ID = $user->Add($arFields)) {
                $arFields['EMAIL'] = 'order' . $arFields['XML_ID'] . '@opticsite.ru';
                if (!$ID = $user->Add($arFields)) {
                    $arFields['EMAIL'] = 'order' . $arFields['XML_ID'] . '@opticsite.ru';
                    pre($arFields);
                    throw new Exception($user->LAST_ERROR);
                }
                pre($arFields);
//                throw new Exception($user->LAST_ERROR);
            }
        }
//        preExit($arData, $arFields);
        Log::success('Обновлен: ', $arFields['LOGIN']);
    }

}
