<?php

namespace Project\Import\Parse\Drupal\Opticsite\Traits;

use CDBResult,
    Project\Import\Parse\Drupal\Opticsite\Model,
    Bitrix\Main\Application,
    Project\Import\View,
    Project\Import\Utility;

trait Node {

    abstract protected function getType();

    static protected function getFieldsNode() {
        return array(
            'nid',
            'nid',
            'vid',
            'type',
            'title',
            'uid',
            'status',
            'created',
            'changed',
            'comment',
            'promote',
        );
    }

    static protected function getFieldsRevisions() {
        return array(
            'vid',
            'uid',
            'title',
            'body',
            'teaser',
            'timestamp',
            'format'
        );
    }

    static public function getConnection() {
        return Application::getConnection(Model\NodeTable::getConnectionName());
    }

    static public function process($page) {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        if ($page == 1) {
            Utility\Report::clear('color');
        }

        $connect = self::getConnection();
        $connect->queryExecute("SET NAMES 'utf8'");
        $connect->queryExecute('SET collation_connection = "utf8_general_ci"');

        $param = array(
            'select' => array(new \Bitrix\Main\Entity\ExpressionField('ID', 'SQL_CALC_FOUND_ROWS %s', 'nid')) + static::getFieldsNode(),
            'filter' => array(
                'type' => self::getType(),
//                'nid' => array(1334),
//                'nid' => array(328),
//                'nid' => array(94, 1226),
//                'nid' => array(899, 900),
//                'nid' => array(432, 425, 593, 594),
//                'nid' => array(2411, 2412, 2413, 2414),
//                'nid' => array(
//                    2411//, 2412, 2413, 2414,
//                    432, 425, 593, 594,
//                    899, 900,
//                    94
//                    ),
                'language' => 'ru',
                '!title' => false,
            ),
            'limit' => $limit,
            'offset' => $start
        );
        $rsData = Model\NodeTable::GetList($param);

        $count = $connect->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            $param = array(
                'select' => static::getFieldsRevisions(),
                'filter' => array(
                    'nid' => $arItem['nid'],
                    'vid' => $arItem['vid']
                ),
            );
            $revData = Model\RevisionsTable::GetList($param);
            $arItem = $revData->Fetch() + $arItem;
            $arItem['img'] = array();
            $res = $connect->query('SELECT f.filepath FROM content_field_image c INNER JOIN files f on(c.field_image_fid=f.fid) WHERE c.nid="' . (int) $arItem['nid'] . '" AND c.vid="' . (int) $arItem['vid'] . '"');
            while ($arImg = $res->Fetch()) {
                $arItem['img'][$arImg['filepath']] = $arImg['filepath'];
            }
            $res = $connect->query('SELECT f.filepath FROM content_field_image_cache c INNER JOIN files f on(c.field_image_cache_fid=f.fid) WHERE c.nid="' . (int) $arItem['nid'] . '" AND c.vid="' . (int) $arItem['vid'] . '"');
            while ($arImg = $res->Fetch()) {
                $arItem['img'][$arImg['filepath']] = $arImg['filepath'];
            }
            $res = $connect->query('SELECT f.filepath FROM content_field_image c INNER JOIN files f on(c.field_image_fid=f.fid) WHERE c.nid="' . (int) $arItem['nid'] . '" AND c.vid="' . (int) $arItem['vid'] . '"');
            while ($arImg = $res->Fetch()) {
                $arItem['img'][$arImg['filepath']] = $arImg['filepath'];
            }
            $arItem['url'] = $connect->query('SELECT u.dst FROM url_alias u WHERE u.src="node/' . (int) $arItem['nid'] . '"')
                    ->fetch();
            self::importProduct($arItem);
        }
//        exit;
        return $pageIsNext;
    }

}
