<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    Cutil,
    CFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    Bitrix\Main\Application,
    Project\Redirect\Redirect,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Articles {

    const LIMIT = 100;

    static protected function getType() {
        return 'story';
    }

    use Traits\Node;

    public static function importProduct($arData) {
        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        if (empty($arData['url']['dst'])) {
            preExit($arData);
        }

        $arData['body'] = Utility\Content::uploadSrcImage($arData['body'], 'http://opticsite.ru');
        $arData['meta'] = Utility\Content::getMeta('http://opticsite.ru/node/' . (int) $arData['nid']);

        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'TIMESTAMP_X' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'DATE_CREATE' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'IBLOCK_ID' => Config::ARTICLES,
            'IBLOCK_SECTION_ID' => 0,
            'NAME' => trim($arData['title']),
            'SORT' => '500',
            'ACTIVE' => $arData['status'] ? 'Y' : 'N',
            'CODE' => Cutil::translit(empty($arData['url']['dst']) ? $arData['title'] : str_replace('articles/', '', $arData['url']['dst']), "ru", array("replace_space" => "-", "replace_other" => "-")),
            'DETAIL_TEXT' => $arData['body'],
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => $arData['teaser'],
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'OLD_NID' => $arData['nid'],
            'OLD_VID' => $arData['vid'],
            'TITLE' => $arData['meta']['title'] ?: '',
            'KEYWORDS' => $arData['meta']['keywords'] ?: '',
            'META_DESCRIPTION' => $arData['meta']['description'] ?: '',
        );
//        preExit($arData, $arFields, $propFields);

        $arItem = Utility\Iblock::searchByName($arFields, $propFields);
        if (empty($arItem)) {
            Log::error('Не найдены статьи', $name);
            return;
        }
        if ($arData['url']['dst']) {
            Redirect::add('/' . $arData['url']['dst'], 'ARTICLES', $arItem['ID']);
        }
//        pre('/' . $arData['url']['dst'], 'ARTICLES', $arItem['ID']);
        Redirect::add('/articles/' . $arData['nid'] .'/', 'ARTICLES', $arItem['ID']);
        Redirect::add('/node/' . $arData['nid'], 'ARTICLES', $arItem['ID']);
//        pre('/node/' . $arData['nid'], 'ARTICLES', $arItem['ID']);
//        preExit();
        $arFields = array();
        if($arData['img']) {
            $img = array_shift($arData['img']);
            if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
                if ($arFile = Utility\Image::upload($img, 'http://opticsite.ru/')) {
                    $arFields["DETAIL_PICTURE"] = $arFile;
                }
            }
            foreach ($arData['img'] as $img) {
                if ($arFile = Utility\Image::upload($img, 'http://opticsite.ru/')) {
                    $propFields["MORE_PHOTO"][] = $arFile;
                }
            }
        }

        Utility\Iblock::update($arItem, $arFields, $propFields);
        Log::success('Добавлены статьи', $arItem['NAME']);
    }

}
