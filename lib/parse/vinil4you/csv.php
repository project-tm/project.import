<?php

namespace Project\Import\Parse\Vinil4you;

use cFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    CCatalogProduct,
    CCatalogSKU,
    CPrice,
    Cutil,
    Project\Core\Redirect,
    Project\Import\Utility\Catalog,
    Project\Import\Settings,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Traits;

class Csv {

    use Traits\Csv;

    static public function importProduct($arData) {
        if (count($arData) != 32) {
//            return;
            preExit($arData);
        }
        exit;
        $isOffers = true;
//        pre($arData);
//        exit;
        $articul = $arData[4];
        $code = $arData[16];
        $quality = (int) str_replace(' ', '', $arData[8]);
        $price = (int) str_replace(' ', '', $arData[5]);
        $priceId = 'RUB';
        $pricePurchase = (int) str_replace(' ', '', $arData[7]);
        $pricePurchaseId = 'RUB';

        $arLang = array("replace_space" => "-", "replace_other" => "-");
        $arFields = array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'NAME' => $arData[0],
            'CODE' => strtolower($code),
            'DETAIL_TEXT' => Catalog::parseContent($arData[2]),
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => Catalog::parseContent($arData[1]),
            'PREVIEW_TEXT_TYPE' => 'html',
        );

        $arItem = Catalog::searchProductByCode($arFields, $propFields);
        Redirect::add('/goods/'.$code, 'PRODUCT', $arItem['ID']);

        $img = array_map('trim', explode(PHP_EOL, $arData[10]));
        if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
            if ($arFile = Catalog::uploadImage($img[0])) {
                $arFields["DETAIL_PICTURE"] = $arFile;
            }
        }
        $propFields = array(
            'OLD_ID' => $arData[18],
            'SEO_TEXT' => $arData[11],
            'TITLE' => $arData[13],
            'KEYWORDS' => $arData[14],
            'META_DESCRIPTION' => $arData[15],
        );

//            pre($img);
        if (count($img) > 1 and empty($arItem['PROPERTY_MORE_PHOTO_VALUE'])) {
            foreach ($img as $key => $value) {
                if ($key) {
                    if ($arFile = Catalog::uploadImage($value)) {
                        $propFields['MORE_PHOTO'][] = $arFile;
                    }
                }
            }
        }
        if ($img and empty($arItem['PROPERTY_MORE_PHOTO_OROG_VALUE'])) {
            foreach ($img as $key => $value) {
                if ($arFile = Catalog::uploadImage($value, true)) {
                    $propFields['MORE_PHOTO_OROG'][] = $arFile;
                }
            }
        }
//            pre($arFields, $propFields);
        Catalog::updateProduct($arItem, $arFields, $propFields);
//        preExit($arFields, $propFields);

        if ($isOffers) {
            $productId = $arItem['ID'];
            $arFields = array(
                'IBLOCK_ID' => Config::OFFERS_ID,
                'NAME' => $arData[0],
                'CODE' => $articul,
            );
            $propFields = array(
                'OLD_PRICE' => (int) str_replace(' ', '', $arData[6]),
                'NOTICE' => $arData[17],
                'OLD_ID' => $arData[18],
                'ARTNUMBER' => $articul,
                'CML2_LINK' => $productId,
            );
//            preExit($arFields, $propFields);
            $arItem = Catalog::searchProductByCode($arFields, $propFields);

            Catalog::saveCatalog($arItem, $isOffers, $quality, $pricePurchase, $pricePurchaseId);
            Catalog::savePrice($arItem, $price, $priceId);
            Catalog::updateProduct($arItem, $arFields, $propFields);


//            preExit($arItem);
        }

//        exit;
        Logs::success('Обновлены товары', $articul);
    }

}
