<?php

namespace Project\Import\Utility\Vinil4you;

use cFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    CCatalogProduct,
    CCatalogSKU,
    CPrice,
    Cutil,
    Project\Core\Redirect,
    Project\Core\Highload,
    Project\Import\Search\Vinil4you\Iblock2Table,
    Project\Import\Search\Vinil4you\Iblock3Table,
    Project\Import\Settings,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Traits;

class Catalog {

    static public function parseContent($content) {
        $content = preg_replace_callback('~src="(http://st.vinyl4you.ru/([^"]*))"~isS', function($preg) {
            if (count($preg) == 3) {
                $file = '/upload/project.import/' . $preg[2];
                $path = $_SERVER["DOCUMENT_ROOT"] . $file;
                if (!file_exists($path)) {
                    CheckDirPath($path);
                    file_put_contents($path, file_get_contents($preg[1]));
                    if (!file_exists($path)) {
                        return $preg[0];
                    }
                }
                return 'src="' . $file . '"';
            }
            return $preg[0];
        }, $content);
        return $content;
    }

    static public function searchHl(&$propFields, &$arParams, $hlName, $param) {
        if (!isset($arParams[$param])) {
            return;
        }
        $id = $value = $arParams[$param];
        unset($arParams[$param]);
        static $arData = array();
        if (!isset($arData[$hlName])) {
            $arData[$hlName]['class'] = Highload::get($hlName);
            $rsData = $arData[$hlName]['class']::getList(array(
                        "select" => array('UF_XML_ID', 'UF_NAME')
            ));
            $rsData = new CDBResult($rsData);
            while ($arItem = $rsData->Fetch()) {
                $arData[$hlName]['data'][$arItem['UF_NAME']] = $arItem['UF_XML_ID'];
            }
        }
        if (!isset($arData[$hlName]['data'][$value])) {
            $arBxData = array(
                'UF_XML_ID' => $id,
                'UF_NAME' => $value
            );
//            pre($arBxData);
            $result = $arData[$hlName]['class']::add($arBxData);
            if (!$result->isSuccess()) {
                throw new \Exception('Ошибка создания');
            }
            $arData[$hlName]['data'][$arBxData['UF_NAME']] = $arBxData['UF_XML_ID'];
        }
        $propFields[$hlName] = $arData[$hlName]['data'][$value];
    }

    static public function updateProduct($arItem, $arFields, $propFields) {
//        pre($arItem['ID'], $arFields, $propFields);
        if ($arFields) {
            $arFields['ACTIVE_FROM'] = '01.05.2017 00:00:00';
            $el = new CIBlockElement;
            $el->Update($arItem['ID'], $arFields);
//            preExit($arItem['ID']);
//            if($arItem['ID']==17246) {
//                preExit($arFields, $arItem);
//            }
        }
        foreach ($propFields as $key => $value) {
            CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], $value, $key);
        }
    }

    static public function uploadImage($url, $orig = false) {
        if ($img = self::_uploadImage($url, $orig)) {
            return $img;
        }
        if ($orig) {
            return self::_uploadImage($url, true);
        }
        return false;
    }

    static public function _uploadImage($url, $orig) {
        if (empty($orig)) {
            $url = str_replace('6e1ffd', 'afacdb', $url);
        }
//        pre($url);
        $file = preg_replace('~(' . preg_quote('http://i') . '[0-9]*' . preg_quote('.vinyl4you.ru/') . ')~iS', '', $url);
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/import/' . $file;
        if (!file_exists($path)) {
            CheckDirPath($path);
            file_put_contents($path, file_get_contents($url));
            if (!file_exists($path)) {
                return false;
            }
        }
        return CFile::MakeFileArray($path);
    }

    static public function saveCatalog($arItem, $isOffers, $quality = null, $pricePurchase = null, $pricePurchaseId = null) {
        $ID = $arItem['ID'];
        $arFields = array();
        $type = $isOffers ? CCatalogSKU::TYPE_PRODUCT : CCatalogSKU::TYPE_FULL;
        if (is_null($arItem['CATALOG_QUANTITY']) or ( !is_null($quality) and $quality != $arItem['CATALOG_QUANTITY'])) {
            $arFields["QUANTITY"] = (int) $quality;
        }
        if ('Y' != $arItem['CATALOG_AVAILABLE']) {
            $arFields["CATALOG_TYPE"] = $isOffers ? CCatalogSKU::TYPE_PRODUCT : CCatalogSKU::TYPE_FULL;
            $arFields["CATALOG_AVAILABLE"] = 'Y';
        }
        if (!is_null($pricePurchase)) {
            if ($pricePurchase != $arItem['CATALOG_PURCHASING_PRICE']) {
                $arFields["PURCHASING_PRICE"] = $pricePurchase;
                $arFields["PURCHASING_CURRENCY"] = $pricePurchaseId;
            }
        }
//        pre($arFields);
        if ($arFields) {
            if ($arItem['CATALOG_PRICE_ID_1']) {
                CCatalogProduct::Update($ID, $arFields);
            } else {
                $arFields['ID'] = $ID;
                //            pre($quality);
                CCatalogProduct::Add($arFields);
            }
        }
    }

    static public function savePrice($arItem, $price, $priceId, $pricePurchase = 0, $pricePurchaseId = '') {
        if ($priceId != 'RUB') {
            preExit($arItem['ID'], $priceId);
        }
        $arFields = array();
        if ($price != $arItem['CATALOG_PRICE_1']) {
            $arFields["PRICE"] = $price;
            $arFields["CURRENCY"] = $priceId;
        }
//        pre($arFields);
        if ($arFields) {
            $arFields['PRODUCT_ID'] = $arItem['ID'];
            if ($arItem['CATALOG_PRICE_ID_1']) {
//                pre($arFields);
                CPrice::Update($arItem['CATALOG_PRICE_ID_1'], $arFields);
            } else {
                $arFields['CATALOG_GROUP_ID'] = Config::PRICE_ID;
//                pre($arFields);
                CPrice::Add($arFields);
            }
        }
    }

    static public function searchProductByCode($arFields) {
        $arSelect = Array("ID");
        $arFilter = Array("IBLOCK_ID" => $arFields['IBLOCK_ID'], 'CODE' => $arFields['CODE']);
        $arItem = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect)->fetch();
        if ($arItem) {
            $arFields['ID'] = $arItem['ID'];
        } else {
            $el = new CIBlockElement;
            if (!$arFields['ID'] = $el->Add($arFields)) {
                preExit($el->LAST_ERROR);
            }
        }
        return self::searchProductById($arFields);
    }

    static public function searchProductByOldId($arFields, $propFields) {
        $arSelect = Array("ID");
        $arFilter = Array("IBLOCK_ID" => $arFields['IBLOCK_ID'], 'PROPERTY_OLD_ID' => $propFields['OLD_ID']);
//        pre($arFilter);
        $arItem = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect)->fetch();
        if ($arItem) {
//            preExit($arItem);
            $arFields['ID'] = $arItem['ID'];
        } else {
//            preExit($arFields, $propFields);
            $el = new CIBlockElement;
            if (!$arFields['ID'] = $el->Add($arFields)) {
                preExit($el->LAST_ERROR);
            }
//            preExit($arFields, $propFields);
        }
        return self::searchProductById($arFields);
    }

    static public function searchProductByArtnumber($arFields, $propFields) {
        $arItem = self::searchProperty($propFields['ARTNUMBER']);
        if (empty($arItem)) {
            $el = new CIBlockElement;
            if (!$arFields['ID'] = $el->Add($arFields)) {
                preExit($el->LAST_ERROR);
            }
            foreach ($propFields as $key => $value) {
                CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], $value, $key);
            }
            return self::searchProductById($arFields);
        } else {
            return self::searchProductById($arItem);
        }
    }

    static public function searchProductById($arItem) {
        $arSelect = Array(
            'ID',
            'ACTIVE',
            'DETAIL_PAGE_URL',
            'DETAIL_PICTURE',
            'PROPERTY_ARTNUMBER',
            'PROPERTY_CML2_LINK',
            'PROPERTY_MORE_PHOTO',
            'PROPERTY_MORE_PHOTO_OROG',
            'CATALOG_GROUP_1',
        );
        $arFilter = Array(
            "IBLOCK_ID" => $arItem['IBLOCK_ID'],
            'ID' => $arItem['ID']
        );
        return CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();
    }

    static public function searchProperty($articul) {
        $param = array(
            'select' => array('ID'),
            'filter' => array(
                'VALUE' => $articul
            ),
        );
        $rsData = Iblock3Table::GetList($param);
        $rsData = new CDBResult($rsData);
        if ($arItem = $rsData->Fetch()) {
            $arItem['IBLOCK_ID'] = Config::OFFERS_ID;
            return $arItem;
        }
//        $param = array(
//            'select' => array('ID'),
//            'filter' => array(
//                'VALUE' => $articul
//            ),
//        );
//        $rsData = Iblock2Table::GetList($param);
//        $rsData = new CDBResult($rsData);
//        if ($arItem = $rsData->Fetch()) {
//            $arItem['IBLOCK_ID'] = Config::OFFERS_ID;
//            return $arItem;
//        }
        return false;
    }

}
