<?php

namespace Project\Import\Utility\Vinil4you;

use Exception;

class Content {

    static private function getFile($file) {
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/project.import/data/' . $file;
        if (!file_exists($path)) {
            CheckDirPath($path);
        }
        return $path;
    }

    static private function getCache($id) {
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/project.import/data/cache/' . $id;
        if (!file_exists($path)) {
            CheckDirPath($path);
        }
        return file_exists($path) ? file_get_contents($path) : false;
    }

    static private function setCache($id, $content) {
//        pre('set-cache');
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/project.import/data/cache/' . $id;
        CheckDirPath($path);
        file_put_contents($path, $content);
    }

    static public function checkRedirect($data) {
        $arForm = array();
        foreach (array('sess_id', 'sess_hash') as $value) {
            preg_match('~' . preg_quote('<input type=hidden name="' . $value . '" value="') . '([a-z0-9]*)' . preg_quote('" />') . '~imsU', $data, $tmp);
//            pre('~' . preg_quote('<input type="hidden" name="' . $value . '" value="') . '([a-z0-9]*)' . preg_quote('" />') . '~imsU', $tmp);
            if (empty($tmp[1])) {
                preHtml($data);
                throw new Exception('не работает редирект');
            }
            $arForm[$value] = $tmp[1];
        }
        if ($arForm) {
            $data = self::curl('http://vinyl4you.ru/admin/login', $arForm);
            return true;
        }
        return false;
    }

    static public function checkAdmin($data, $id) {
        return strpos($data, '<input type="hidden" name="goods_id" value="' . $id . '" id="goods_id_field" />');
    }

    static public function get($id, &$arUser) {
        if ($data = self::getCache($id)) {
//            pre('get-cache');
            return $data;
        }

        $data = self::curl('https://vinyl4you.storeland.ru/admin/store_goods_edit/' . $id);
        if (!self::checkAdmin($data, $id)) {
//            pre(__LINE__);
            if (empty($arUser['hash'])) {
//                pre(__LINE__);
                $data = self::curl('https://storeland.ru/user/login');
                preg_match('~' . preg_quote('<input type="hidden" name="hash" value="') . '([a-z0-9]*)' . preg_quote('" />') . '~imsU', $data, $tmp);
                if (empty($tmp[1])) {
//                    pre(__LINE__);
                    if (!self::checkRedirect($data)) {
//                        pre($tmp);
//                        preHtml($data);
                        throw new Exception('не работает автологин');
                    }
                } else {
//                    pre(__LINE__);
                    $arUser['hash'] = $tmp[1];
                    $data = self::curl('https://storeland.ru/user/login', $arUser);
                    if (!self::checkRedirect($data)) {
//                        pre(__LINE__);
//                        pre($tmp);
//                        preHtml($data);
                        throw new Exception('не работает авторизация');
                    }
                }
            }
//            pre(__LINE__);
            $data = self::curl('https://vinyl4you.storeland.ru/admin/store_goods_edit/' . $id);
        }

//        preHtml($data);
//        exit;
        self::setCache($id, $data);
        return $data;
    }

    static public function curl($url, $arPost = false, $isJson = false) {
        pre($url);


//        preExit(file_get_contents(self::getFile('cookie')));

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'catalog/1.0');
        if ($arPost) {
            curl_setopt($curl, CURLOPT_POST, true);
            if ($isJson) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($arPost));
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            } else {
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($arPost));
            }
        }
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_COOKIEFILE, self::getFile('cookie')); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, self::getFile('cookie')); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        curl_close($curl); #Завершаем сеанс cURL

        file_put_contents(
                self::getFile('cookie'), str_replace('#HttpOnly_', '', file_get_contents(self::getFile('cookie')))
        );

        return $isJson ? json_decode($out, true) : $out;
    }

}
