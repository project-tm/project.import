<?php

namespace Project\Import\Parse;

use Exception,
    CDBResult,
    CIBlockElement,
    Bitrix\Main\Entity,
    Bitrix\Main\Application,
    Project\Import\Search\ElementTable,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\View,
    Project\Import\Log;

class Active {

    const LIMIT = 100;

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;

        $date = '17.01.2017 00:00:00';
        $param = array(
            'select' => array(
                new Entity\ExpressionField('FOUND_ROWS', 'SQL_CALC_FOUND_ROWS %s', 'ID'),
                'ID',
                'NAME',
                'ACTIVE_FROM',
            ),
            'filter' => array(
                'IBLOCK_ID' => Config::CATALOG_ID,
                'ACTIVE' => 'Y',
                '!ACTIVE_FROM' => $date
            ),
            'limit' => $limit,
            'offset' => 0
        );
        
        $rsData = ElementTable::GetList($param);
        $rsData = new CDBResult($rsData);

        $count = Application::getConnection()->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        View::notProcessed($count);
        $pageIsNext = ($limit * $page) < $count;

        $el = new CIBlockElement;
        while ($arItem = $rsData->Fetch()) {
            $arFields = array();
            $arFields['ACTIVE_FROM'] = $date;
            $el->Update($arItem['ID'], $arFields);
            Log::success('Обновлены товары', $arItem['NAME']);
        }
        return $pageIsNext;
    }

}
