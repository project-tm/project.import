<?php

namespace Import\Catalog\Search\Westtech;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

Loc::loadMessages(__FILE__);

class Iblock3Table extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element_prop_s3';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'column_name' => 'IBLOCK_ELEMENT_ID',
                    )),
            new Main\Entity\StringField('VALUE', array(
                'column_name' => 'PROPERTY_32',
            )),
        );
    }

}
