<?php

namespace Import\Catalog\Search\Westtech;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class Iblock2Table extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element_prop_s2';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'column_name' => 'IBLOCK_ELEMENT_ID',
                    )),
            new Main\Entity\StringField('ARTNUMBER', array(
                'column_name' => 'PROPERTY_9',
            )),
            new Main\Entity\StringField('ARTICUL', array(
                'column_name' => 'PROPERTY_89',
            )),
            new Main\Entity\StringField('ARTICUL2', array(
                'column_name' => 'PROPERTY_90',
            )),
            new Main\Entity\StringField('ARTICUL3', array(
                'column_name' => 'PROPERTY_91',
            )),
            new Main\Entity\StringField('ARTICUL4', array(
                'column_name' => 'PROPERTY_92',
            )),
            new Main\Entity\StringField('OLD_ID', array(
                'column_name' => 'PROPERTY_172',
            )),
        );
    }

}
