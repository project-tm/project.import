<?php

namespace Project\Import\Search\Westtech;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class SubcategTable extends DataManager {

    public static function getConnectionName() {
        return 'old';
    }

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'subcateg';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('id', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('alias')
        );
    }

}
