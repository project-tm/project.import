<?php

namespace Project\Import\Search\Westtech;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class ItemTable extends DataManager {

    public static function getConnectionName() {
        return 'old';
    }

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('id', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('item'),
            new Main\Entity\StringField('alias'),
            new Main\Entity\IntegerField('categ'),
            new Main\Entity\IntegerField('subcat'),
            new Main\Entity\IntegerField('brand'),
            new Main\Entity\IntegerField('enable'),
            new Main\Entity\StringField('articul'),
            new Main\Entity\StringField('articul_2'),
            new Main\Entity\StringField('articul_3'),
            new Main\Entity\StringField('articul_4'),
        );
    }

}
