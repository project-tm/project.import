<?php

namespace Import\Catalog\Parse\Westtech;

use Exception,
    CFile,
    CIBlockElement,
    CIBlockSection,
    Import\Catalog\Search\Westtech\ItemTable,
    Import\Catalog\Report,
    Import\Catalog\Config,
    Import\Catalog\Import\Logs,
    Import\Catalog\Traits,
    Import\Catalog\Utility;

class Product {

    const LIMIT = 100;

    use Traits\Westtech\Catalog;

    public static function importProduct($arData) {
        if (empty($arData['alias']) or empty($arData['item'])) {
            return;
        }

        $categ = $arData['categ'];
        $section = $arData['subcat'];

        $arFilter = Array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'CODE' => $arData['alias'],
        );
        $arSelect = Array(
            'ID',
            'ACTIVE_FROM',
            'NAME',
            'PROPERTY_OLD_ID',
        );
        $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();
        if (empty($arItem)) {
//            pre($arData);
            $record = \Bitrix\Main\Application::getConnection('old')
                    ->query('SELECT i.*, c.alias AS cAlias, c.categ AS cName, s.alias AS sAlias, s.subcat AS sName, b.name AS bName, b.currency as bCurrency '
                            . 'FROM items i LEFT JOIN category c on(i.categ=c.id) LEFT JOIN subcateg s on(i.subcat=s.id) LEFT JOIN brands b on(i.brand=b.id) '
                            . 'WHERE i.id="' . $arData['id'] . '"')
                    ->fetch();

            $record = array_map(function($str) {
                return iconv('cp1251', 'utf-8', $str);
            }, $record);
//            pre($record);
            $arFilter = Array(
                "IBLOCK_ID" => Config::IBLOCK_ID,
                'CODE' => $record['alias'],
            );
            pre($arFilter);
            $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();
            if (empty($arItem)) {
                $arFilter = Array(
                    "IBLOCK_ID" => Config::IBLOCK_ID,
                    'NAME' => $record['item'],
                );
                pre($arFilter);
                $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();
            }
            if (empty($arItem)) {
                $arFilter = Array(
                    "IBLOCK_ID" => Config::IBLOCK_ID,
                    'PROPERTY_OLD_ID' => $record['id'],
                );
                pre($arFilter);
                $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();
            }
            if (empty($arItem)) {
                $arFilter = Array(
                    "IBLOCK_ID" => Config::IBLOCK_ID,
                    'PROPERTY_ARTNUMBER' => "WT-" . str_pad(htmlspecialchars($record['id']), 8, '0', STR_PAD_LEFT),
                );
                pre($arFilter);
                $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();
            }
            if (empty($arItem)) {
                $arFields = array(
                    'ACTIVE_FROM' => '01.06.2017 00:00:00',
                    'IBLOCK_ID' => Config::IBLOCK_ID,
                    'IBLOCK_SECTION_ID' => 0,
                    'NAME' => $record['item'],
                    'SORT' => '500',
                    'CODE' => $record['alias'],
                    'DETAIL_TEXT' => $record['description'],
                    'DETAIL_TEXT_TYPE' => 'html',
                    'PREVIEW_TEXT' => $record['short_description'],
                    'PREVIEW_TEXT_TYPE' => 'html',
                );
                $propFields = array(
                    'OLD_ID' => $record['id'],
                    'ARTNUMBER' => "WT-" . str_pad(htmlspecialchars($record['id']), 8, '0', STR_PAD_LEFT),
                    'NEWPRODUCT' => $record['novick'] ? 1 : 0,
                    'SALELEADER' => $record['hit'] ? 2 : 0,
                    'SPECIALOFFER' => $record['deliv'] ? 3 : 0,
                    'articul' => $record['articul'],
                    'articul_2' => $record['articul_2'],
                    'articul_3' => $record['articul_3'],
                    'articul_4' => $record['articul_4'],
                    'item_keywords' => self::filterMeta($record, $record['item_keywords']),
                    'meta_description' => self::filterMeta($record, $record['meta_description']),
                    'meta_title' => self::filterMeta($record, $record['meta_title']),
                    'tag' => $record['tag'],
                    'brand' => $record['brand'],
                );

                if ($record['recommend']) {
                    $arFilter = Array(
                        "IBLOCK_ID" => 2,
                        "PROPERTY_OLD_ID" => explode(',', $record['recommend']),
                    );
                    $arSelect = array(
                        "ID",
                    );
                    $propFields['RECOMMEND'] = array();
                    $resProd = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                    while ($arProduct = $resProd->Fetch()) {
                        $propFields['RECOMMEND'][] = $arProduct['ID'];
                    }
//                preExit($propFields);
                }

                $resBrand = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 7, 'PROPERTY_OLDid' => $record['brand']), false, false, Array("ID"));
                if ($arBrand = $resBrand->GetNext()) {
                    $propFields['MANUFACTURER'] = $arBrand['ID'];
                } else {
                    preExit('brand');
                }

                $resImg = \Bitrix\Main\Application::getConnection('old')->query('SELECT * FROM images WHERE item_id="' . $arData['id'] . '"');
                while ($img = $resImg->Fetch()) {
                    $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . '/images/items/subcat' . $record['subcat'] . '/' . $img['path']);
                    if ($arFile) {
                        if (empty($arFields["DETAIL_PICTURE"])) {
                            $arFields["DETAIL_PICTURE"] = $arFields["PREVIEW_PICTURE"] = $arFile;
                        } else {
                            $propFields['MORE_PHOTO'][] = $arFile;
                        }
                    }
                }
//                preExit($arFields, $propFields);

                $arItem = Utility\Catalog::add($arFields);

                switch ($record['bCurrency']) {
                    case "usd":
                        $currency = "USD";
                        break;
                    case "eur":
                        $currency = "EUR";
                        break;
                    case "gbp":
                        $currency = "GBP";
                        break;
                    default:
                        $currency = "RUB";
                }
                Utility\Catalog::savePrice($arItem, $record['price'], $currency);

                $quality = max($record['quantity'], $record['quantity_2'], $record['quantity_3'], $record['quantity_4']);
                Utility\Catalog::saveCatalog($arItem, false, $quality);
                Utility\Catalog::updateProduct($arItem, $arFields, $propFields);

//            pre($arFields, $propFields, $record);
                Report::add('product.add', "http://old.west-tech.ru/catalog/" . implode('/', array($record['cAlias'], $record['sAlias'], $record['alias'])));

                Utility\Westtech\Catalog::importSection($arItem, $record['categ'], $record['subcat']);
//            preExit($arItem);
//            exit;

                Logs::success('Добавлены товары', $arItem['NAME']);
            }
        }
    }

    static private function filterMeta($recored, $item) {
        $item = str_replace(array(
            '{subcat}', '{cat}', '{brand}'
                ), array(
            '{sAlias}', '{cName}', '{bName}'
                ), $item);
        foreach ($recored as $key => $value) {
            $item = str_replace('{' . $key . '}', $value, $item);
        }
        return $item;
    }

}
