<?php

namespace Import\Catalog\Parse\Westtech;

use Exception,
    CIBlockElement,
    Import\Catalog\Config,
    Import\Catalog\Import\Logs,
    Import\Catalog\Traits,
    Import\Catalog\Utility,
    Import\Catalog\Search\Westtech\Iblock2Table;

class Articul {

    const LIMIT = 100;

    use Traits\Westtech\Catalog;

    public static function importProduct($arData) {
        $record = \Bitrix\Main\Application::getConnection('old')
                ->query('SELECT i.*, c.alias AS cAlias, c.categ AS cName, s.alias AS sAlias, s.subcat AS sName, b.name AS bName, b.currency as bCurrency '
                        . 'FROM items i LEFT JOIN category c on(i.categ=c.id) LEFT JOIN subcateg s on(i.subcat=s.id) LEFT JOIN brands b on(i.brand=b.id) '
                        . 'WHERE i.id="' . $arData['id'] . '"')
                ->fetch();

        $record = array_map(function($str) {
            return iconv('cp1251', 'utf-8', $str);
        }, $record);

//        pre($record);

        $arFilter = Array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'CODE' => $arData['alias'],
        );
        $arSelect = Array(
            'ID'
        );
//        pre($arFilter);
//        $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();

        if (empty($arItem)) {
            $arFilter = array(
                "LOGIC" => "OR",
                "ARTNUMBER" => 'WT-' . str_pad(htmlspecialchars($record['id']), 8, '0', STR_PAD_LEFT)
            );
            if ($record['id']) {
                $arFilter['OLD_ID'] = $record['id'];
            }
            if ($record['articul']) {
                $arFilter['ARTICUL'] = $record['articul'];
            }
            if ($record['articul_2']) {
                $arFilter['ARTICUL2'] = $record['articul_2'];
            }
            if ($record['articul_3']) {
                $arFilter['ARTICUL3'] = $record['articul_3'];
            }
            if ($record['articul_4']) {
                $arFilter['ARTICUL4'] = $record['articul_4'];
            }
            $param = array(
                'select' => array('ID'),
                'filter' => array(
                    $arFilter
                ),
            );
//            pre($arFilter);
            $arItem = Iblock2Table::GetList($param)->Fetch();
        }

        if ($arItem) {
            $arFilter = Array(
                "IBLOCK_ID" => Config::IBLOCK_ID,
                'ID' => $arItem['ID'],
            );
            $arSelect = Array(
                'ID',
                'IBLOCK_ID',
                'ACTIVE_FROM',
                'NAME',
                'PROPERTY_OLD_ID',
                'PROPERTY_ARTICUL',
                'PROPERTY_ARTICUL_2',
                'PROPERTY_ARTICUL_3',
                'PROPERTY_ARTICUL_4',
            );
            $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->Fetch();

            $arFields = array();
            $propFields = array();
            if (empty($arItem['PROPERTY_ARTICUL_VALUE']) and $record['articul']) {
                $propFields['ARTICUL'] = $record['articul'];
            }
            if (empty($arItem['PROPERTY_ARTICUL_2_VALUE']) and $record['articul_2']) {
                $propFields['ARTICUL_2'] = $record['articul_2'];
            }
            if (empty($arItem['PROPERTY_ARTICUL_3_VALUE']) and $record['articul_3']) {
                $propFields['ARTICUL_3'] = $record['articul_3'];
            }
            if (empty($arItem['PROPERTY_ARTICUL_4_VALUE']) and $record['articul_4']) {
                $propFields['ARTICUL_4'] = $record['articul_4'];
            }

            if ($propFields) {
//                preExit($propFields);
                Utility\Catalog::updateProduct($arItem, $arFields, $propFields);
            }
//            preExit($arItem);
            Logs::success('Обновлены товары', $arItem['NAME']);
        } else {
//            preExit();
            Logs::error('Не найдены товары', $record['item']);
        }
    }

    static private function filterMeta($recored, $item) {
        $item = str_replace(array(
            '{subcat}', '{cat}', '{brand}'
                ), array(
            '{sAlias}', '{cName}', '{bName}'
                ), $item);
        foreach ($recored as $key => $value) {
            $item = str_replace('{' . $key . '}', $value, $item);
        }
        return $item;
    }

}
