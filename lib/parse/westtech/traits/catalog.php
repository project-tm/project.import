<?php

namespace Import\Catalog\Traits\Westtech;

use CDBResult,
    Import\Catalog\Search\Westtech\ItemTable,
    Bitrix\Main\Application,
    Import\Catalog\View;

trait Catalog {

    static public function getFieldsCatalog() {
        return array(
            'id',
            'id',
            'item',
            'alias',
            'categ',
            'subcat'
        );
    }

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        Application::getConnection('old')->queryExecute("SET NAMES 'cp1251'");
        Application::getConnection('old')->queryExecute('SET collation_connection = "cp1251_general_ci"');

        $param = array(
            'select' => array(new \Bitrix\Main\Entity\ExpressionField('FOUND_ROWS', 'SQL_CALC_FOUND_ROWS %s', 'id')) + static::getFieldsCatalog(),
            'filter' => array(
//                'subcat' => 78,
//                'id' => '182637',
                //'item' => 'Brahner BP-160/WH',
                //'!alias' => false,
                //'enable' => array(1),
            ),
            'limit' => $limit,
            'offset' => $start
        );
        $rsData = ItemTable::GetList($param);
//        $rsData = new CDBResult($rsData);

        $count = Application::getConnection('old')->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
//            pre($arItem);
            self::importProduct($arItem);
//            preExit($arItem);
        }
        return $pageIsNext;
    }

}
