<?php

namespace Import\Catalog\Parse\Westtech;

use CDBResult,
    CIBlockElement,
    CCatalogProduct,
    CCatalogSKU,
    CPrice,
    Project\Import\Traits,
    Project\Import\Settings,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\Search\PropsTable,
    Project\Import\Search\Westtech\Iblock2Table,
    Project\Import\Log;

class Price {

    use Traits\Csv;

    public static function importProduct($arData) {
        $atrNumber = $arData[0];
        if(empty($atrNumber)) {
            return;
        }
        $price = (int) str_replace(' ', '', $arData[4]);
        $quality = (int) $arData[6];
//        preExit($arData);
//        $filter = function($prortyId, $value) {
//            $param = array(
//                'select' => array('IBLOCK_ELEMENT_ID'),
//                'filter' => array(
//                    'IBLOCK_ELEMENT_ID' => $prortyId,
//                    'VALUE' => $value
//                ),
//            );
//            $rsData = PropsTable::GetList($param);
//            $rsData = new CDBResult($rsData);
//            if ($arItem = $rsData->Fetch()) {
//                return $arItem['IBLOCK_ELEMENT_ID'];
//            }
//        };
        if (1) {
            $param = array(
                'select' => array('ID'),
                'filter' => array(
//                    'ID' => $prortyId,
                    array(
                        "LOGIC" => "OR",
//                        "ARTNUMBER" => 'WT-' . $atrNumber,
                        "ARTICUL" => $atrNumber,
                        "ARTICUL2" => $atrNumber,
                        "ARTICUL3" => $atrNumber,
                        "ARTICUL4" => $atrNumber,
                    ),
                ),
            );
//            pre($param);
            $rsData = Iblock2Table::GetList($param);
            if ($arItem = $rsData->Fetch()) {
                $ID = $arItem['ID'];
            }
        } else {
//            $ID = $filter([
//                Config::PROPERTY_ARTICUL,
//                Config::PROPERTY_ARTICUL2,
//                Config::PROPERTY_ARTICUL3,
//                Config::PROPERTY_ARTICUL4
//                    ], $atrNumber);
//            if (empty($ID)) {
//                $ID = $filter(Config::PROPERTY_ARTNUMBER, 'WT-' . $atrNumber);
//            }
        }

        if (empty($ID)) {
            Logs::error('Не найдены артикулы', $atrNumber);
            return;
        }
//        preExit($ID);
//        preExit($ID, $atrNumber);
        $arSelect = Array(
            'ID',
            'ACTIVE',
            'DETAIL_PAGE_URL',
            'PROPERTY_ARTNUMBER',
            'CATALOG_GROUP_1',
        );
        $arFilter = Array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'ID' => $ID
        );
        $arItem = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect)->GetNext();
        Logs::success('Обновлены артикулы', $atrNumber);
//        $ID = $arItem['ID'];
        if (Data::get('PERCENT')) {
            $price *= Settings::get('percent');
        }
        if (Data::get('PRICE') and $price != $arItem['CATALOG_PRICE_1']) {
//                    pre('seracrh price');
            if ($arItem['CATALOG_PRICE_ID_1']) {
                if ($price != $arPrice['PRICE']) {
                    $arFields = Array(
                        "PRODUCT_ID" => $ID,
                        "CATALOG_GROUP_ID" => Config::PRICE_ID,
                        "PRICE" => $price,
                        "CURRENCY" => 'RUB',
                    );
//                            pre($arPrice, $arFields);
                    CPrice::Update($arItem['CATALOG_PRICE_ID_1'], $arFields);
                }
            } else {
                $arFields = Array(
                    "PRODUCT_ID" => $ID,
                    "CATALOG_GROUP_ID" => Config::PRICE_ID,
                    "PRICE" => $price,
                    "CURRENCY" => 'RUB',
                );
//                        pre($arFields);
                CPrice::Add($arFields);
            }
        }

        if (Data::get('QUALITY') and $quality != $arItem['CATALOG_QUANTITY']) {
            pre('seracrh quality');
            if ($arCatalog = CCatalogProduct::GetByID($ID)) {
                if ($quality != $arCatalog['QUANTITY']) {
                    $arFields = array(
                        //                            "CATALOG_TYPE" => CCatalogSKU::TYPE_CATALOG,
                        //                            "CATALOG_AVAILABLE" => 'Y',
                        "QUANTITY" => $quality,
                    );
//                            pre($arCatalog, $quality);
                    CCatalogProduct::Update($ID, $arFields);
                }
            } else {
                $arFields = array(
                    "ID" => $ID,
//                            "CATALOG_TYPE" => CCatalogSKU::TYPE_CATALOG,
//                            "CATALOG_AVAILABLE" => 'Y',
                    "QUANTITY" => $quality,
                );
//                        pre($quality);
                CCatalogProduct::Add($arFields);
            }
        }
    }

}
