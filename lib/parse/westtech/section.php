<?php

namespace Import\Catalog\Parse\Westtech;

use CDBResult,
    Exception,
    CIBlockElement,
    CIBlockSection,
    Import\Catalog\Search\Westtech\ItemTable,
    Bitrix\Main\Application,
    Import\Catalog\View,
    Import\Catalog\Config,
    Import\Catalog\Import\Logs,
    Import\Catalog\Traits,
    Import\Catalog\Utility\Westtech\Catalog;

class Section {

    const LIMIT = 100;

    use Traits\Westtech\Catalog;

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        Application::getConnection('old')->queryExecute("SET NAMES 'cp1251'");
        Application::getConnection('old')->queryExecute('SET collation_connection = "cp1251_general_ci"');

        $param = array(
            'select' => array(new \Bitrix\Main\Entity\ExpressionField('FOUND_ROWS', 'SQL_CALC_FOUND_ROWS %s', 'id')) + static::getFieldsCatalog(),
            'filter' => array(
//                'subcat' => 78,
//                'item' => 'Behringer CL208',
                '!alias' => false,
                'enable' => array(1),
            ),
            'order' => array(
                'categ' => 'ASC',
                'subcat' => 'ASC',
            ),
            'limit' => $limit,
            'offset' => $start
        );
        $rsData = ItemTable::GetList($param);
        $rsData = new CDBResult($rsData);

        $count = Application::getConnection('old')->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        $arResult = array();
        $arResult2 = array();
        while ($arItem = $rsData->Fetch()) {
            if (empty($arItem['alias'])) {
                continue;
            }
            if (empty($arItem['item'])) {
                continue;
            }
            $categ = $arItem['categ'];
            $section = $arItem['subcat'];
            if ($section) {
                $sectionID = Catalog::getSectionId($section, $categ);
            } elseif ($categ) {
                $sectionID = Catalog::getSectionId($categ);
            } else {
                continue;
            }
            $arItem['item'] = iconv('cp1251', 'utf-8', $arItem['item']);
            $arResult[$sectionID][$arItem['id']] = $arItem['id'];
            $arResult2[$sectionID][$arItem['item']] = $arItem['item'];
        }

        foreach ($arResult as $sectionID => $list) {
            self::importProduct($sectionID, $list);
        }
        foreach ($arResult2 as $sectionID => $list) {
            self::importProduct2($sectionID, $list);
        }
        return $pageIsNext;
    }

    public static function importProduct($sectionID, $list) {
        $arFilter = Array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'PROPERTY_OLD_ID' => $list,
//            'ACTIVE' => 'Y',
            '!SECTION_ID' => $sectionID,
        );
//        pre($arFilter);
        $arSelect = Array(
            'ID',
            'NAME',
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $is = false;
        while ($arItem = $res->fetch()) {
//            preExit($arItem);
            if (Catalog::importSection($arItem, $sectionID)) {
                Logs::success('Обновлены товары', $arItem['NAME']);
            }
        }
    }

    public static function importProduct2($sectionID, $list) {
        $arFilter = Array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'NAME' => $list,
//            'ACTIVE' => 'Y',
            '!SECTION_ID' => $sectionID,
        );
//        preExit($arFilter);
        $arSelect = Array(
            'ID',
            'NAME',
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $is = false;
        while ($arItem = $res->fetch()) {
//            preExit($arItem);
            if (Catalog::importSection($arItem, $sectionID)) {
                Logs::success('Обновлены товары', $arItem['NAME']);
            }
        }
    }

}
