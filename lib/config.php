<?php

namespace Project\Import;

use Bitrix\Main\Loader;

class Config {

    const MODULE = 'project.import';
    const CATALOG_ID = \Tm\Opticsite\Iblock\Config::CATALOG_ID;
    const OFFERS_ID = \Tm\Opticsite\Iblock\Config::OFFERS_ID;
    const ARTICLES = \Tm\Opticsite\Iblock\Config::ARTICLES;
    const FAQ = \Tm\Opticsite\Iblock\Config::FAQ;
    const COMMENT = \Tm\Opticsite\Iblock\Config::COMMENT;
    const PRICE_ID = 1;
    const LIMIT = 30;
    const ROUTER = array(
//        'Vinil4you\Yandex' => 'Импорт из яндекс товаров',
//        'Vinil4you\Csv' => 'Импорт csv',
//        'Vinil4you\Product' => 'Импорт параметров товаров',
//        'Westtech\Product' => 'Добавление товаров со старой базы',
//        'Westtech\Price' => 'Импорт цены и количества',
//        'Westtech\Section' => 'Импорт разделов',
        'Drupal\Opticsite\Faq' => 'Импорт faq',
        'Drupal\Opticsite\Articles' => 'Импорт статей',
        'Drupal\Opticsite\Highload' => 'Импорт списков',
        'Drupal\Opticsite\Fields' => 'Импорт полей',
        'Drupal\Opticsite\Product' => 'Импорт товаров',
        'Drupal\Opticsite\User' => 'Импорт пользователей',
        'Drupal\Opticsite\Order' => 'Импорт заказов',
        'Drupal\Opticsite\Comment' => 'Импорт комментариев',
        'Active' => 'Поправить активность',
    );

}
