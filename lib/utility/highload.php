<?php

namespace Project\Import\Utility;

use CDBResult;

class Highload {

    static private $arClass = array();
    static private $arData = array();
    static private $arOldData = array();

    static private function table($name) {
        if (isset(self::$arClass[$name])) {
            return self::$arClass[$name];
        } else {
            return self::$arClass[$name] = \Project\Core\Highload::get($name);
        }
    }

    static public function get($hlName, $arFields) {
        return self::$arData[$hlName][$arFields['UF_NAME']] ?: false;
    }

    static public function getOld($hlName, $arFields) {
        return self::$arOldData[$hlName][$arFields['UF_OLD_ID']] ?: false;
    }

    static private function set($hlName, $arFields) {
        self::$arData[$hlName][$arFields['UF_NAME']] = $arFields;
        if(is_array($arFields['UF_OLD_ID'])) {
            foreach ($arFields['UF_OLD_ID'] as $value) {
                self::$arOldData[$hlName][$value] = $arFields;
            }
        }
    }

    static private function init($hlName) {
        if (!isset($arData[$hlName])) {
            $rsData = self::table($hlName)::getList(array(
                        "select" => array('ID', 'UF_XML_ID', 'UF_NAME', 'UF_OLD_ID')
            ));
            while ($arItem = $rsData->Fetch()) {
                self::set($hlName, $arItem);
            }
        }
    }

    static public function update($hlName, $arFields) {
        self::init($hlName);
        if ($arItem = self::get($hlName, $arFields)) {
            self::table()::update($arItem['ID'], $arFields);
        } else {
            $result = self::table()::add($arFields);
            if (!$result->isSuccess()) {
                throw new \Exception('Ошибка создания');
            }
            $arFields['ID'] = $result->getId();
            self::set($hlName, $arFields);
        }
    }

    static public function searchOldId($hlName, $key, $value = false) {
        self::init($hlName);
        if (empty($value)) {
            $value = 'нет значения: ' . $key;
        }
        $arFields = array(
            'UF_OLD_ID' => $key,
            'UF_XML_ID' => $key,
            'UF_NAME' => $value,
        );
        if ($arItem = self::getOld($hlName, $arFields)) {
//            pre($arItem['UF_XML_ID']);
            return $arItem['UF_XML_ID'];
        } else {
            return false;
            preExit($hlName, $key, $value, $arFields);
            $result = self::table($hlName)::add($arFields);
            if (!$result->isSuccess()) {
                throw new \Exception('Ошибка создания');
            }
            $arFields['ID'] = $result->getId();
            self::set($hlName, $arFields);
            return $arFields['UF_XML_ID'];
        }
    }

    static public function search($hlName, $key, $value = false) {
        self::init($hlName);
        if (empty($value)) {
            $value = 'нет значения: ' . $key;
        }
        $arFields = array(
            'UF_XML_ID' => $key,
            'UF_NAME' => $value,
        );
        if ($arItem = self::get($hlName, $arFields)) {
            return $arItem['UF_XML_ID'];
        } else {
//            preExit($arFields);
            $result = self::table($hlName)::add($arFields);
            if (!$result->isSuccess()) {
                throw new \Exception('Ошибка создания');
            }
            $arFields['ID'] = $result->getId();
            self::set($hlName, $arFields);
            return $arFields['UF_XML_ID'];
        }
    }

}
