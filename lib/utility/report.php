<?php

namespace Project\Import\Utility;

class Report {

    private static function getPath($type) {
        return $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/project.import/log/' . $type . '.txt';
    }

    public static function clear($type) {
        pre(self::getPath($type));
        if(file_exists(self::getPath($type))) {
            unlink(self::getPath($type));
        }
    }

    public static function add($type, $message) {
        CheckDirPath(self::getPath($type));
        file_put_contents(self::getPath($type), $message . PHP_EOL . file_get_contents(self::getPath($type)));
    }

}
