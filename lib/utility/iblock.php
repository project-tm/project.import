<?php

namespace Project\Import\Utility;

use Exception,
    CIBlockElement;

class Iblock {

    static protected $arSelect = Array(
        'ID',
        'IBLOCK_ID',
        'ACTIVE',
        'NAME',
        'DETAIL_PAGE_URL',
        'DETAIL_PICTURE',
        'PROPERTY_MORE_PHOTO',
    );

    static public function add($arFields) {
        $el = new CIBlockElement;
        if (!$arFields['ID'] = $el->Add($arFields)) {
            preExit($el->LAST_ERROR);
        }
        return self::searchProductById($arFields);
    }

    static public function update($arItem, $arFields, $propFields) {
        if ($arFields) {
            $el = new CIBlockElement;
            $el->Update($arItem['ID'], $arFields);
        }
        unset($propFields['COLOR_TEXT']);
//        pre($propFields);
        foreach ($propFields as $key => $value) {
            CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], $value, $key);
        }
    }

    static public function searchByFilter($arFilter, $arFields, $propFields, $isUpdate = true) {
        $arItem = CIBlockElement::GetList(Array(), $arFilter, false, false, static::$arSelect)->fetch();
        if ($arItem) {
            if ($isUpdate) {
                self::update($arItem, $arFields, $propFields);
            }
            return self::searchById($arItem);
        } else {
            $el = new CIBlockElement;
            if (!$arFields['ID'] = $el->Add($arFields)) {
//                pre($arFilter, $arFields, $propFields);
                throw new Exception($el->LAST_ERROR);
            }
//            pre($propFields);
            foreach ($propFields as $key => $value) {
                CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], $value, $key);
            }
            return self::searchById($arFields);
        }
    }

    static public function searchByName($arFields, $propFields) {
        $arFilter = array("IBLOCK_ID" => $arFields['IBLOCK_ID'], 'NAME' => $arFields['NAME']);
        return self::searchByFilter($arFilter, $arFields, $propFields);
    }

    static public function searchById($arItem) {
        $arFilter = Array(
            "IBLOCK_ID" => $arItem['IBLOCK_ID'],
            'ID' => $arItem['ID']
        );
        return CIBlockElement::GetList(array(), $arFilter, false, false, static::$arSelect)->Fetch();
    }

}
