<?php

namespace Project\Import;

class Time {

    public static function start() {
        $_SESSION[__CLASS__] = array();
        $_SESSION[__CLASS__]['start'] = microtime(true);
    }

    public static function stop() {
        $_SESSION[__CLASS__]['stop'] = microtime(true);
    }

    public static function get() {
        return ($_SESSION[__CLASS__]['stop'] ?: microtime(true)) - $_SESSION[__CLASS__]['start'];
    }

}
