<?php

namespace Project\Import;

use cFile,
    Bitrix\Main\Application;

class Router {

    public static function pase($page) {
        pre($page, Data::get('FILE'), Data::get('FILE_TYPE'));

        if (empty(Data::get('FILE_TYPE'))) {
            Log::error('Ошибка', 'Не указан тип импорта');
        } else {
            $action = Data::get('FILE_TYPE');
            if (isset(Config::ROUTER[$action])) {
                $action = '\Project\Import\Parse\\' . $action;
                if ($action::process($page)) {
                    return true;
                }
            } else {
                Log::error('Ошибка', 'Выбран не верный тип импорта');
            }
        }
        if(Data::get('FILE')) {
            cFile::Delete(Data::get('FILE'));
        }
        return false;
    }

    public static function process() {
        if (self::get()) {
            global $APPLICATION;
            $request = Application::getInstance()->getContext()->getRequest();
            if ($request->get('page') == 'stop') {
                self::clear();
                $APPLICATION->RestartBuffer();
            } elseif ($request->get('page') and $request->get('page') != self::get()) {
                self::set((int) $request->get('page'));
            }
            if (self::pase(self::get())) {
                self::next();
                $url = $APPLICATION->GetCurPageParam('page=' . self::get(), array('page'));
                View::stop($APPLICATION->GetCurPageParam('page=stop', array('page')));
            } else {
                self::clear();
                Time::stop();
                $url = $APPLICATION->GetCurPageParam('page=end', array('page'));
            }
            View::redirect($url);
        }
    }

    public static function start($data) {
        self::set(1);
        Time::start();
        Data::set($data);
        Log::clear();
    }

    public static function get() {
        return $_SESSION[__CLASS__] ?: 0;
    }

    public static function set($data) {
        $_SESSION[__CLASS__] = $data;
    }

    public static function next() {
        $_SESSION[__CLASS__] ++;
    }

    public static function clear() {
        unset($_SESSION[__CLASS__]);
        Data::clear();
    }

}
