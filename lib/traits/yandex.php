<?php

namespace Project\Import\Traits;

use SimpleXMLElement,
    cFile,
    Exception,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\Log;

trait Yandex {

    static public function process($page) {
        $limit = Config::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $filename = $_SERVER["DOCUMENT_ROOT"] . cFile::GetPath(Data::get('FILE'));
        if (file_exists($filename)) {
            try {
                $xml = new SimpleXMLElement(file_get_contents($filename));
                static::importSections($xml->shop->categories);
                $total = $xml->shop->offers->offer->count();
                echo '<h3>Выполнено ' . round(($page - 1) / ceil($total / $limit) * 100, 2) . '% (' . $start . '/' . $total . ')</h3>';
                $key = 1;

                foreach ($xml->shop->offers->children() as $offers) {
                    $key++;
                    if ($key < $start) {
                        continue;
                    }
                    if ($key >= $end) {
                        return true;
                    }
                    static::importProduct($offers);
                }
            } catch (Exception $exc) {
                Logs::error('Ошибка при импорте', $exc->getTraceAsString());
            }
        }
        return false;
    }

}
