<?php

use Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Import\Import\ImportTable;

IncludeModuleLangFile(__FILE__);

class project_import extends CModule {

    var $MODULE_ID = 'project.import';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_IMPORT_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_IMPORT_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_IMPORT_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallDB();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /*
     * InstallDB
     */

    public function InstallDB() {
        ImportTable::tableCreate();
    }

    public function UnInstallDB() {
        ImportTable::tableDrop();
    }

}
