# Модуль импорта #

необходим модуль https://bitbucket.org/project-tm/project.core https://github.com/DigitalWand/digitalwand.admin_helper

* [Vinil4you\Csv](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/vinil4you/csv.php)  обновление цен и остатков из csv
* [Vinil4you\Yandex](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/vinil4you/yandex.php)  загрузка товаров из яндекс.маркета 
* [Vinil4you\Product](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/vinil4you/product.php)  загрузка товаров из storeland.ru (имитация авторизации админа в системе)
* [Active](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/active.php) обновление ACTIVE_FROM для товаров
* [Westtech\Product](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/westtech/product.php) обновление разделов у товаров из бд другого сайта
* [Westtech\Section](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/westtech/section.php) импорт разделов товаров из бд другого сайта
* [Westtech\Price](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/westtech/price.php)
* [Drupal\Opticsite\Faq](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/drupal/opticsite/faq.php) Импорт faq из друпала
* [Drupal\Opticsite\Articles](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/drupal/opticsite/articles.php) Импорт статей из друпала
* [Drupal\Opticsite\Highload](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/drupal/opticsite/highload.php) Импорт списков из друпала
* [Drupal\Opticsite\Fields](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/drupal/opticsite/fields.php) Импорт полей из друпала
* [Drupal\Opticsite\Product](https://bitbucket.org/project-tm/project.import/src/166bcbf762258189576ea3d88dceec9de686ba0e/lib/parse/drupal/opticsite/product.php) Импорт товаров из друпала

![h2kGxYgH.png](https://bitbucket.org/repo/8zXRa9q/images/677225705-h2kGxYgH.png)

/bitrix/admin/sale_discount_catalog_migrator.php?lang=ru