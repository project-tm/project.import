<?php

use Bitrix\Main\Loader;

Loader::includeModule('tm.opticsite');
Loader::includeModule('project.redirect');
Loader::includeModule('iblock');
Loader::includeModule('highloadblock');
Loader::includeModule('catalog');
Loader::includeModule('sale');
